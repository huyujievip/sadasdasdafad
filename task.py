import re
import time

import connector
import utils
import config
from game import bank
from game.zjh import GameStatus

def money_return_task():
    # 清理游戏
    for db in [1, 4, 5, 6, 7, 8, 9, 12, 13, 14, 17]:
        try:
            print("操作数据库：{}".format(db))
            r = connector.get_connection(db)
            key_list = r.keys()
            print(key_list)
            print("开始遍历key_list")
            for key in key_list:
                print(key)
                rst = re.match(r"^(-?\d+)_(\d+)$", key)
                print(rst)
                if rst is not None:
                    group_id = rst.group(1)
                    game_id = rst.group(2)
                    print(group_id, game_id)
                    status, participant = r.hmget("{}_{}".format(group_id, game_id), ['status', 'participant'])
                    if db != 15:
                        if status != "created" and status != "choosing":
                            continue
                    else:
                        if status != "created":
                            continue
                    participant = utils.string_to_list(participant)
                    if len(participant) == 0:
                        continue
                    for user in participant:
                        total_amount = int(r.hget("{}_{}_{}".format(group_id, game_id, user), "total_amount"))
                        print(total_amount)
                        r.hset("{}_{}".format(group_id, game_id), "status", "end")
                        utils.add_user_balance(user, total_amount)
                        try:
                            utils.send_message(user,
                                               "您之前参与的游戏存在未结算的场次，现已退回 ${}".format(utils.balance_format(total_amount)))
                            time.sleep(3)
                        except Exception as e:
                            print(e)
            #r.flushdb()
        except Exception as e:
            print(e)
    # 炸金花退款
    for db in [18, 19, 20]:
        try:
            print("操作数据库：{}".format(db))
            r = connector.get_connection(db)
            key_list = r.keys()
            print(key_list)
            print("开始遍历key_list")
            for key in key_list:
                print(key)
                rst = re.match(r"^(-?\d+)_(\d+)$", key)
                print(rst)
                if rst is not None:
                    group_id = rst.group(1)
                    game_id = rst.group(2)
                    print(group_id, game_id)
                    status, participant = r.hmget("{}_{}".format(group_id, game_id), ['status', 'participant'])
                    if status != GameStatus.WAITING_START.value:
                        continue
                    participant = utils.string_to_list(participant)
                    if len(participant) == 0:
                        continue
                    for user in participant:
                        total_amount = int(r.hget("{}_{}_{}".format(group_id, game_id, user), "total_amount"))
                        print(total_amount)
                        r.hset("{}_{}".format(group_id, game_id), "status", GameStatus.END.value)
                        utils.add_user_balance(user, total_amount)
                        try:
                            utils.send_message(user,
                                               "您之前参与的游戏存在未结算的场次，现已退回 ${}".format(utils.balance_format(total_amount)))
                            time.sleep(3)
                        except Exception as e:
                            print(e)
            #r.flushdb()
        except Exception as e:
            print(e)
    # 开始红包退款
    try:
        r = connector.get_connection(2)
        key_list = r.keys()
        print(key_list)
        print("开始遍历key_list")
        for key in key_list:
            print(key)
            rst = re.match(r"^lucky_(-?\d+)_(\d+)$", key)
            print(rst)
            if rst is not None:
                group_id = rst.group(1)
                game_id = rst.group(2)
                print(group_id, game_id)
                rest_red_pocket_list = r.lrange("lucky_{}_{}".format(group_id, game_id), 0, -1)
                master_id = r.get("lucky_{}_{}_master_id".format(group_id, game_id))
                total_amount = 0
                #for amount in rest_red_pocket_list:
                    #total_amount += int(amount)
                #utils.add_user_balance(master_id, total_amount)
                #try:
                    #utils.send_message(master_id,
                                       #"您之前发送的红包未被全部领取，剩余的金额现已退回 ${}".format(utils.balance_format(total_amount)))
                    #time.sleep(3)
                #except Exception as e:
                    #print(e)
                    #68-76行删除则红包数据清除且不退回
        r.flushdb()
    except Exception as e:
        print(e)


def bank_everyday_task():
    r = connector.get_connection(11)
    user_list = r.keys()
    for user in user_list:
        user_ck = r.get(user)
        bonus = int(float(user_ck) * bank.RATE)
        new_ck = int(user_ck) + bonus
        r.set(user, new_ck)
        # print("用户存款利息计算完成，存款 {}，当日利息 {}，本金加利息 {}".format(utils.balance_format(user_ck), utils.balance_format(bonus),
        #                                                  utils.balance_format(new_ck)))


def rank_task():
    try:
        r = connector.get_connection(0)
        rank_db = connector.get_connection(16)
        # 开始分析 财富榜
        user_list = r.keys("*")
        list2 = []
        for user in user_list:
            name, balance, id = r.hmget(user, ["name", "balance", "id"])
            balance = int(balance)
            if balance < 100000000 or int(id) in config.RANK_EXCLUDE_LIST:
                continue
            list2.append([name[:min(6, len(name))], balance, id])
        if len(list2) != 0:
            list2.sort(key=lambda x: x[1], reverse=True)
            list2 = list2[:min(20, len(list2))]
        else:
            list2 = []
        # 数据入库
        rank_db.set("rank_list", str(list2))

        # 开始分析 大神榜单
        list3 = []
        for user in user_list:
            name, win_amount, id = r.hmget(user, ["name", "win_amount", "id"])
            win_amount = int(win_amount)
            list3.append([name[:min(6, len(name))], win_amount, id])
        list3.sort(key=lambda x: x[1], reverse=True)
        list3 = list3[:min(10, len(list3))]
        # 数据入库
        rank_db.set("shen_list", str(list3))

        # 开始分析用户排名
        list4 = []
        for user in user_list:
            name, balance, id = r.hmget(user, ["name", "balance", "id"])
            if int(id) in config.RANK_EXCLUDE_LIST:
                continue
            list4.append([name[:min(6, len(name))], int(balance), id])
            list4.sort(key=lambda x: x[1], reverse=True)
        position = 1
        if len(list4) != 0:
            for i in list4:
                # print(i[2], position)
                rank_db.set(i[2], position)
                position += 1
        # print("分析完毕")
    except Exception as e:
        print(e)
