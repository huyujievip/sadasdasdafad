from game.lp import lp_handler
from game.bs import bs_handler
from game.lucky import lucky_handler
from game.dt import dt_handler
from game.yc import yc_handler
from game.pj import pj_handler
from game.yx import yx_handler
from game.hh import hh_handler
from game.bj import bj_handler
from game.bank import ck_handler, qk_handler
from game.erba import erba_handler
from game.bg import bg_handler
from game.jisusuoha import jisusuoha_handler
from game.bk import bk_handler
from game.wk import wk_handler
from game.zjh import zjh_handler
from game.zjh2 import zjh2_handler
from game.zjh3 import zjh3_handler
from global_handlers import global_button_dispatcher_handler, set_title_handler, zz_handler, rank_handler, \
    shen_handler, lh_handler, jb_handler, admin_balance_add_handler, admin_balance_reduce_handler, user_info_handler, \
    g_info_handler, admin_all_balance_add_handler, admin_all_balance_reduce_handler
from telegram.ext import Updater
from config import TOKEN
import threading
import schedule
from utils import load_ad_to_redis, write_black_list_to_file, load_black_list_to_redis, send_award, check_schedule
from task import money_return_task, bank_everyday_task, rank_task
# 将硬盘文件加载到 Redis
load_black_list_to_redis()

# 启动广告更新线程
threading.Thread(target=load_ad_to_redis).start()

# 启动 Redis 黑名单写回文件线程
threading.Thread(target=write_black_list_to_file).start()

# 设置奖励发放任务，清空用户统计数据
schedule.every().monday.at('08:00').do(send_award)
# schedule.every(10).seconds.do(send_award)

# 每天 06:50 返回未结算游戏、未领完红包、清理内存
schedule.every().day.at('22:17').do(money_return_task)
# schedule.every(15).seconds.do(money_return_task)

# 每天计算银行利息
schedule.every().day.at('06:55').do(bank_everyday_task)
# schedule.every(15).seconds.do(bank_everyday_task)

# 每 10 分钟计算排名信息
schedule.every(10).minutes.do(rank_task)


# 启动任务自动监控线程
threading.Thread(target=check_schedule).start()

updater = Updater(token=TOKEN, use_context=True, workers=5)
dispatcher = updater.dispatcher

dispatcher.add_handler(global_button_dispatcher_handler)
dispatcher.add_handler(set_title_handler)
# dispatcher.add_handler(test_handler)
dispatcher.add_handler(lh_handler)
dispatcher.add_handler(jb_handler)
dispatcher.add_handler(ck_handler)
dispatcher.add_handler(qk_handler)
dispatcher.add_handler(user_info_handler)
dispatcher.add_handler(g_info_handler)
dispatcher.add_handler(admin_balance_add_handler)
dispatcher.add_handler(admin_all_balance_add_handler)
dispatcher.add_handler(admin_balance_reduce_handler)
dispatcher.add_handler(admin_all_balance_reduce_handler)
dispatcher.add_handler(lucky_handler)
dispatcher.add_handler(zz_handler)
dispatcher.add_handler(erba_handler)
dispatcher.add_handler(yc_handler)
dispatcher.add_handler(dt_handler)
dispatcher.add_handler(zjh_handler)
dispatcher.add_handler(zjh2_handler)
dispatcher.add_handler(zjh3_handler)
dispatcher.add_handler(pj_handler)
dispatcher.add_handler(bg_handler)
dispatcher.add_handler(yx_handler)
dispatcher.add_handler(bj_handler)
dispatcher.add_handler(hh_handler)
dispatcher.add_handler(rank_handler)
dispatcher.add_handler(shen_handler)
dispatcher.add_handler(lp_handler)
dispatcher.add_handler(bs_handler)
dispatcher.add_handler(jisusuoha_handler)
dispatcher.add_handler(bk_handler)
dispatcher.add_handler(wk_handler)

updater.start_polling()
updater.idle()
