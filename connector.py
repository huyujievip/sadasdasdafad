import redis

pool0 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=0)  # 用户数据库
pool1 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=1)  # lunpan
pool2 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=2)  # lucky
pool3 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=3)  # 创建新游戏锁
pool4 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=4)  # yuanchen
pool5 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=5)  # longhu
pool6 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=6)  # paijiu
pool7 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=7)  # yuxia
pool8 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=8)  # honghei
pool9 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=9)  # baijia
pool10 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=10)  # AD
pool11 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=11)  # 存款数据库
pool12 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=12)  # 28
pool13 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=13)  # 水果机
pool14 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=14)  # 梭哈轮盘
pool15 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=15)  # 21点
pool16 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=16)  # 排名缓存数据库
pool17 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=17)  # 副本轮盘
pool18 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=18)  # 炸金花
pool19 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=19)  # 炸金花2
pool20 = redis.ConnectionPool(host='127.0.0.1', port=6379, encoding='utf8', decode_responses=True, db=20)  # 炸金花3


def get_connection(db):
    if db == 0:
        return redis.StrictRedis(connection_pool=pool0)
    elif db == 1:
        return redis.StrictRedis(connection_pool=pool1)
    elif db == 2:
        return redis.StrictRedis(connection_pool=pool2)
    elif db == 3:
        return redis.StrictRedis(connection_pool=pool3)
    elif db == 4:
        return redis.StrictRedis(connection_pool=pool4)
    elif db == 5:
        return redis.StrictRedis(connection_pool=pool5)
    elif db == 6:
        return redis.StrictRedis(connection_pool=pool6)
    elif db == 7:
        return redis.StrictRedis(connection_pool=pool7)
    elif db == 8:
        return redis.StrictRedis(connection_pool=pool8)
    elif db == 9:
        return redis.StrictRedis(connection_pool=pool9)
    elif db == 10:
        return redis.StrictRedis(connection_pool=pool10)
    elif db == 11:
        return redis.StrictRedis(connection_pool=pool11)
    elif db == 12:
        return redis.StrictRedis(connection_pool=pool12)
    elif db == 13:
        return redis.StrictRedis(connection_pool=pool13)
    elif db == 14:
        return redis.StrictRedis(connection_pool=pool14)
    elif db == 15:
        return redis.StrictRedis(connection_pool=pool15)
    elif db == 16:
        return redis.StrictRedis(connection_pool=pool16)
    elif db == 17:
        return redis.StrictRedis(connection_pool=pool17)
    elif db == 18:
        return redis.StrictRedis(connection_pool=pool18)
    elif db == 19:
        return redis.StrictRedis(connection_pool=pool19)
    elif db == 20:
        return redis.StrictRedis(connection_pool=pool20)
