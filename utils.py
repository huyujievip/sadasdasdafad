import random
import re
import time

import ast

import schedule
import telegram
from telegram.ext import CommandHandler

import connector
import utils
from config import TOKEN, RANK_EXCLUDE_LIST

bot = telegram.Bot(token=TOKEN)


class Message(object):
    def __init__(self, group_id, group_type, user_id, uname, **kw):
        self.group_id = group_id
        self.group_type = group_type
        self.user_id = user_id
        self.name = uname[:min(6, len(uname))]


class QueryData(object):
    def __init__(self, game_name, group_id, action, arg1, arg2, arg3, arg4):
        self.game_name = game_name
        self.group_id = group_id
        self.action = action
        self.arg1 = arg1
        self.arg2 = arg2
        self.arg3 = arg3
        self.arg4 = arg4

    def params_to_list(self):
        return [self.game_name, self.group_id, self.action, self.arg1, self.arg2, self.arg3, self.arg4]


class User(object):
    def __init__(self, id, balance, last_sign, last_win, win_amount, name):
        self.id = id
        self.balance = balance
        self.last_sign = last_sign
        self.last_win = last_win
        self.win_amount = win_amount
        self.name = name


def escaped_text(text):
    rst = re.sub(r"(\.|\||\#|\*|\(|\)|\>|\{|\}|\[|\]|\_|\~|\`|\+|\-|\=|\!|\\|\~|)", "", str(text))
    return rst


def load_ad_to_redis():
    while True:
        try:
            with open("ad.txt", "r") as file:
                content = file.read()
            file.close()
            # print(content)
            r = connector.get_connection(10)
            r.set("ad", content)
            time.sleep(5)
        except Exception as e:
            print(e)
            print("广告文件读取错误")
            time.sleep(5)


def get_random_ad():
    try:
        r = connector.get_connection(10)
        ad = r.get("ad")
        # print(ad)
        rst = re.findall(r"(\[.*?\]\s?\[.*?\]\(.*?\))", ad)
        if len(rst) != 0:
            random_ad = rst[random.randint(0, len(rst) - 1)]
            return random_ad
        else:
            return ""
    except Exception as e:
        print(e)
        print("Redis 获取广告错误")
        return "[广告] [【默认】](https://t.me/gameyyds)"


def add_user_balance(user_id, amount):
    r = connector.get_connection(0)
    if check_user_exists(user_id):
        balance = get_user_balance(user_id)
        new_balance = balance + int(amount)
        r.hmset(user_id, {"balance": new_balance})
    else:
        r.hmset(user_id, {"id": user_id, "balance": amount, "last_sign": 1621677665, "title": -1, "name": "无名氏",
                          "last_win": 1621677665, "win_amount": 0})


def reduce_user_balance(user_id, amount):
    r = connector.get_connection(0)
    if check_user_exists(user_id):
        balance = get_user_balance(user_id)
        if balance < int(amount):
            new_balance = 0
        else:
            new_balance = balance - int(amount)
            if new_balance < 0:
                new_balance = 0
        r.hmset(user_id, {"balance": new_balance})
    else:
        r.hmset(user_id, {"id": user_id, "balance": 0, "last_sign": 1621677665, "title": -1, "name": "无名氏",
                          "last_win": 1621677665, "win_amount": 0})


def check_user_exists(user_id):
    r = connector.get_connection(0)
    return r.exists(user_id)


def get_user_balance(user_id):
    r = connector.get_connection(0)
    if check_user_exists(user_id):
        return int(r.hget(user_id, "balance"))
    else:
        r.hmset(user_id, {"id": user_id, "balance": 0, "last_sign": 1621677665, "title": -1, "name": "无名氏",
                          "last_win": 1621677665, "win_amount": 0})
        return 0


def get_user_balance_update(user_id, update):
    message = ana_message(update)
    r = connector.get_connection(0)
    if check_user_exists(user_id):
        r.hset(user_id, "name", message.name)
        return int(r.hget(user_id, "balance"))
    else:
        r.hmset(user_id, {"id": user_id, "balance": 0, "last_sign": 1621677665, "title": -1, "name": message.name,
                          "last_win": 1621677665, "win_amount": 0})
        return 0


def get_game_id(group_id):
    return group_id + "_" + round(time.time() * 1000)


def get_redis_user_info(user_id):
    r = connector.get_connection(0)
    user_info = r.hmget(user_id, ["id", "balance", "last_sign", "last_win", "win_amount", "name"])
    return User(user_info[0], user_info[1], user_info[2], user_info[3], user_info[4], user_info[5])


def show_balance(update, context):
    user_id = update.effective_user.id
    check_user_exists(user_id)
    get_user_balance_update(user_id, update)
    r = connector.get_connection(0)
    user_info = r.hmget(user_id, ["id", "balance", "last_sign", "last_win", "win_amount", "name"])
    user = User(user_info[0], user_info[1], user_info[2], user_info[3], user_info[4], user_info[5])
    title = get_title(user.id)
    query = update.callback_query
    list1 = rank_list(update, context)
    list2 = shen_list(update, context)
    order = 1
    in_balance_list = 0
    for list1_item in list1:
        if user.id == list1_item[2]:
            in_balance_list = 1
            break
        else:
            order += 1
    order2 = 1
    in_win_list = 0
    for list2_item in list2:
        if user.id == list2_item[2]:
            in_win_list = 1
            break
        else:
            order2 += 1
    if in_win_list:
        text1 = "(第{}名)".format(order2)
    else:
        text1 = "(未上榜)"
    if in_balance_list:
        text2 = "第{}名".format(order)
    else:
        text2 = "未上榜"
    text = "【{}】{}\n当前余额：{}\n本周累计胜场：{} {}\n财富榜：{}".format(title, user.name, balance_format(user.balance),
                                                          user.win_amount, text1, text2)
    query.answer(text=text, show_alert=True)


def rank_list(update, context):
    try:
        rank_db = connector.get_connection(16)
        print("------------------------------------------")
        print(utils.string_to_list(rank_db.get("rank_list")))
        return utils.string_to_list(rank_db.get("rank_list"))
    except Exception as e:
        print(e)


def get_balance_order(user_id):
    try:
        if int(user_id) in RANK_EXCLUDE_LIST:
            return "未知"
        get_user_balance(user_id)
        rank_db = connector.get_connection(16)
        order = rank_db.get(user_id)
        print("------------------------------------------")
        print(order)
        if str(order) != "None":
            return int(order)
        else:
            return "未知"
    except Exception as e:
        print(e)


def shen_list(update, context):
    rank_db = connector.get_connection(16)
    # print("------------------------------------------")
    # print(utils.string_to_list(rank_db.get("shen_list")))
    return utils.string_to_list(rank_db.get("shen_list"))


def get_random_money():
    return random.randint(3500, 6800) * 100


def increase_win_amount(user_id, money):
    r = connector.get_connection(0)
    if check_user_exists(user_id):
        if int(money) >= 20000:
            last_win, win_amount, = r.hmget(user_id, ["last_win", "win_amount"])
            last_win = int(last_win)
            win_amount = int(win_amount)
            print("用户胜场数：{}".format(win_amount))
            now = int(time.time())
            if now - last_win > 50:
                print("满足条件，胜场数加1，更新时间")
                r.hmset(user_id, {"last_win": now, "win_amount": win_amount + 1})
            else:
                print("不满足条件")


def sign(update, context):
    user_id = update.effective_user.id
    r = connector.get_connection(0)
    get_user_balance_update(user_id, update)
    last_sign_time = int(r.hget(user_id, "last_sign"))
    now_time = int(time.time())
    query = update.callback_query
    if now_time - last_sign_time > 100:
        random_money = get_random_money()
        balance = get_user_balance_update(user_id, update)
        if balance > 50000000:
            query.answer(text="您当前的资金（＞5千万）充足，不可签到", show_alert=True)
            return
        new_balance = int(balance) + random_money
        r.hset(user_id, "balance", new_balance)
        r.hset(user_id, "last_sign", now_time)
        text = '''
        签到成功
        系统赠送了您：${}
        当前总余额：${}
        棱哈一时爽，请理性游戏~
        每80秒可再次点签到领取
        '''.format(balance_format(random_money), balance_format(new_balance))
        query.answer(text=text, show_alert=True)
    else:
        text = "80秒内限定签到一次"
        query.answer(text=text, show_alert=True)


def get_timestamp():
    return int(str(round(time.time() * 1000))[5:])


def list_to_string(alist):
    return str(alist)


def string_to_list(string):
    return ast.literal_eval(string)


def ana_message(update):
    group_type = update.effective_chat.type
    user_id = update.effective_user.id
    group_id = update.effective_message.chat_id
    try:
        username = update.effective_user.username
    except Exception as e:
        username = update.effective_user.id
    user = update.effective_user
    firstname = str(user["first_name"])
    lastname = str(user["last_name"])
    name = ""
    if firstname != "None":
        name = firstname + " "
    if lastname != "None":
        name += lastname
    if len(name) == 0:
        name = username
    name = name[:min(6, len(name))]
    name = escaped_text(name)
    return Message(group_id, group_type, user_id, name)


def ana_query_data(update):
    query = update.callback_query
    try:
        rst = re.match("(.*?)_(.*?)_(.*?)_(.*?)_(.*?)_(.*?)_(.*)", query.data)
        if rst is None:
            query.answer(text="发生错误，请稍后重试")
            return
        else:
            game_name, group_id, action, arg1, arg2, arg3, arg4 = rst.groups()
            return QueryData(game_name, group_id, action, arg1, arg2, arg3, arg4)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")
        return


def show_msg(update, msg, isAlert):
    query = update.callback_query
    query.answer(text=msg, show_alert=isAlert)


def get_title(user_id):
    r = connector.get_connection(0)
    if r.hget(user_id, "title") == "-1":
        balance = int(get_user_balance(user_id))
        base = 1
        if balance == 0:
            return "一贫如洗"
        elif 0 < balance < 1000000 * base:
            return "囊中羞涩"
        elif 1000000 * base <= balance < 10000000 * base:
            return "自给自足"
        elif 10000000 * base <= balance < 100000000 * base:
            return "丰衣足食"
        elif 100000000 * base <= balance < 1000000000 * base:
            return "小康之家"
        elif 1000000000 * base <= balance < 10000000000 * base:
            return "一掷千金"
        elif 10000000000 * base <= balance < 100000000000 * base:
            return "腰缠万贯"
        elif 100000000000 * base <= balance < 1000000000000 * base:
            return "富可敌国"
        elif 1000000000000 * base <= balance:
            return "宇宙首富"
    else:
        return escaped_text(r.hget(user_id, "title"))


def send_message(user_id, text):
    bot.sendMessage(
        chat_id=user_id,
        text=text
    )


def send_md_message(user_id, text):
    bot.sendMessage(
        chat_id=user_id,
        text=text,
        parse_mode="Markdown"
    )


def balance_format(balance):
    balance = int(balance)
    flag = 0
    out = ""
    if balance == 0:
        return "0"
    while balance != 0:
        num = balance % 10
        balance = int(balance / 10)
        # print(num, balance)
        flag += 1
        if flag == 5:
            out = str(num) + "," + out
            flag = 1
        else:
            out = str(num) + out
        # print(out)
    return out


def set_new_game_lock(group_id):
    r = connector.get_connection(3)
    r.set("{}_new_game_lock".format(group_id), "-1", ex=90)


def new_game_lock_existed(group_id):
    r = connector.get_connection(3)
    return r.exists("{}_new_game_lock".format(group_id))


def del_new_game_lock(group_id):
    r = connector.get_connection(3)
    r.delete("{}_new_game_lock".format(group_id))


def is_user_in_black_list(user_id):
    try:
        user_id = int(user_id)
        r = connector.get_connection(10)
        black_list = ast.literal_eval(r.get("black_list"))
        int_bl = []
        for user in black_list:
            int_bl.append(int(user))
        if user_id in int_bl:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        print("检测用户是否存在黑名单错误")
        return False


def write_black_list_to_file():
    while True:
        try:
            r1 = connector.get_connection(10)
            redis_black_list = ast.literal_eval(r1.get("black_list"))
            with open("blacklist.txt", "w") as file:
                for id in redis_black_list:
                    file.write(str(id) + "\n")
                file.close()
            time.sleep(5)
        except Exception as e:
            print(e)
            print("将黑名单写入到文件中错误")
            time.sleep(5)


def load_black_list_to_redis():
    try:
        with open("blacklist.txt", "r") as file:
            content = file.read()
        file.close()
        r2 = connector.get_connection(10)
        rst = re.findall(r"(\d+)\s?", content)
        r2.set("black_list", str(rst))
    except Exception as e:
        print(e)
        print("加载黑名单用户到 Redis 错误")


def format_markdown(text):
    text = str(text).replace("(", "\(")
    text = str(text).replace("(", "\(")
    text = str(text).replace("(", "\(")
    text = str(text).replace("(", "\(")
    text = str(text).replace("(", "\(")


def send_award():
    try:
        r = connector.get_connection(0)
        user_list = r.keys("*")
        list2 = []
        for user in user_list:
            name, win_amount, id = r.hmget(user, ["name", "win_amount", "id"])
            win_amount = int(win_amount)
            list2.append([name[:min(6, len(name))], win_amount, id])
            r.hset(user, "win_amount", 0)
        list2.sort(key=lambda x: x[1], reverse=True)
        list2 = list2[:min(10, len(list2))]
        award_list = [0, 111, 99, 88, 77, 66, 55, 44, 33, 22, 11]
        award_base = 100000000
        index = 1
        for user in list2:
            user_id = int(user[2])
            add_user_balance(user_id, award_list[index] * award_base)
            try:
                send_message(user_id,
                             "恭喜您，在上周中您的表现优异，获得第 {} 名的优异成绩，获得 {}E 奖励，奖励已到账，本周请再接再厉！".format(
                                 index, award_list[index]
                             ))
            except Exception as e:
                print(e)
                print("奖励信息发送失败")
            index += 1
        print("奖励已全部发放完毕")
    except Exception as e:
        print(e)


def get_now_second_timestamp():
    return int(str(round(time.time() * 1000))[:-3])


def check_schedule():
    while True:
        schedule.run_pending()
        time.sleep(1)


def end_game_check(update, game_id, db):
    message = ana_message(update)
    chose_user = 0
    r = connector.get_connection(db)
    status, num, participant = r.hmget("{}_{}".format(message.group_id, game_id), ['status', 'num', 'participant'])
    print("获取到 Redis 中的数据", status, num, participant)
    participant = string_to_list(participant)
    if status != "choosing":
        show_msg(update, "此局游戏已经结算！由于系统限制，显示文字刷新失败，但是结算金额已经到账！", True)
        # query = update.callback_query
        # query.edit_message_text(
        #     text="此局游戏已经结算！由于系统限制，显示文字刷新失败，但是结算金额已经到账！",
        #     parse_mode="MarkdownV2",
        # )
        return False
    print("开始判断用户是否参与了此局游戏")
    if message.user_id not in participant:
        show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return False
    print("判断是否存在冷却时间")
    if r.exists("{}_{}_end_lock".format(message.group_id, game_id)):
        print("当前处在冷却时间")
        show_msg(update, "请等待无用户操作6s后结算游戏", True)
        return False
    print("开始遍历用户")
    for user in participant:
        print("判断用户是否做出选择")
        if r.hget("{}_{}_{}".format(message.group_id, game_id, user), "choose_num") != "-1":
            print("此用户已经做出了选择")
            chose_user += 1
    print("判断是否满足终止游戏条件")
    if chose_user == len(participant):
        return True
    if chose_user == 0 and r.exists("{}_{}_time_limit".format(message.group_id, game_id)):
        print("当前终止游戏条件不满足")
        show_msg(update, "当前没有用户做出选择，请等待用户选择或者选择阶段结束后结束游戏", True)
        return False
    end_time_limit = int(r.ttl("{}_{}_time_limit".format(message.group_id, game_id)))
    if chose_user > 0 and end_time_limit > 30:
        print("当前终止游戏条件不满足，有人未选择，并且选择时间未超过30s")
        show_msg(update, "当前还有用户未作出选择，请等待全部用户选择完毕或者开局30s后结算", True)
        return False
    return True

