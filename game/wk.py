from telegram.ext import MessageHandler, Filters
import utils
import random
from telegram.ext.dispatcher import run_async

# 需要开启挖矿功能的群组
GROUPS = [-1001364210054,-1001416342610,-1001206823993,-1001346064364,-1001186431967]

# 控制中奖比例，如果填50，那么中奖几率就是 1/50，即 2%；填写 100，中奖几率就是 1/100，即 1%
RATE = 100
# 每次奖金
BONUS = 1888888888


@run_async
def wk(update, context):
    try:
        message = utils.ana_message(update)
        print(message.user_id)
        chat_type = update.effective_chat.type
        if message.group_type == "private":
            return
        if int(message.group_id) not in GROUPS:
            return
        random_int = random.randint(1, RATE)
        print(random_int)
        if random_int == RATE:
            balance = utils.get_user_balance(message.user_id)
            utils.add_user_balance(message.user_id, BONUS)
            user_info = utils.get_redis_user_info(message.user_id)
            update.message.reply_text(
                "🎉🎉🎉*恭喜 {} 在聊天中获得系统随机赠送游戏积分 ${}*".format(user_info.name, utils.balance_format(BONUS)),
                parse_mode="MarkdownV2"
            )

    except Exception as e:
        print(e)


wk_handler = MessageHandler(Filters.text, wk)
