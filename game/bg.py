import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random
import ast

bot = telegram.Bot(token=TOKEN)


def make_keyboard1(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("1%",
                                 callback_data=str(
                                     'bg_{}_addMoney_0.01_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5%",
                                 callback_data=str(
                                     'bg_{}_addMoney_0.05_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20%",
                                 callback_data=str(
                                     'bg_{}_addMoney_0.2_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50%",
                                 callback_data=str(
                                     'bg_{}_addMoney_0.5_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("80%",
                                 callback_data=str(
                                     'bg_{}_addMoney_0.8_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("梭哈",
                                 callback_data=str(
                                     'bg_{}_addMoney_1.0_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2W", callback_data=str(
                'bg_{}_addMoney_20000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5W", callback_data=str(
                'bg_{}_addMoney_50000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20W", callback_data=str(
                'bg_{}_addMoney_200000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50W", callback_data=str(
                'bg_{}_addMoney_500000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200W", callback_data=str(
                'bg_{}_addMoney_2000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500W", callback_data=str(
                'bg_{}_addMoney_5000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2KW", callback_data=str(
                'bg_{}_addMoney_20000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KW", callback_data=str(
                'bg_{}_addMoney_50000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2E", callback_data=str(
                'bg_{}_addMoney_200000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5E", callback_data=str(
                'bg_{}_addMoney_500000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20E", callback_data=str(
                'bg_{}_addMoney_2000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50E", callback_data=str(
                'bg_{}_addMoney_5000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("200E", callback_data=str(
                'bg_{}_addMoney_20000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500E", callback_data=str(
                'bg_{}_addMoney_50000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KE", callback_data=str(
                'bg_{}_addMoney_200000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KE", callback_data=str(
                'bg_{}_addMoney_500000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2WE", callback_data=str(
                'bg_{}_addMoney_2000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5WE", callback_data=str(
                'bg_{}_addMoney_5000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("20WE", callback_data=str(
                'bg_{}_addMoney_20000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50WE", callback_data=str(
                'bg_{}_addMoney_50000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200WE", callback_data=str(
                'bg_{}_addMoney_200000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500WE", callback_data=str(
                'bg_{}_addMoney_500000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KWE", callback_data=str(
                'bg_{}_addMoney_2000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5kWE", callback_data=str(
                'bg_{}_addMoney_5000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("1EE", callback_data=str(
                'bg_{}_addMoney_10000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("3EE", callback_data=str(
                'bg_{}_addMoney_30000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5EE", callback_data=str(
                'bg_{}_addMoney_50000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("10EE", callback_data=str(
                'bg_{}_addMoney_100000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50EE", callback_data=str(
                'bg_{}_addMoney_500000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("99EE", callback_data=str(
                'bg_{}_addMoney_990000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("▶️开始游戏",
                                 callback_data=str(
                                     'bg_{}_startGame_{}_None_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
            InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
        ]
    ]


def make_keyboard2(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("🍎",
                                 callback_data=str(
                                     'bg_{}_choosingNum_苹果_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("🍊",
                                 callback_data=str(
                                     'bg_{}_choosingNum_橙子_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("🍓",
                                 callback_data=str(
                                     'bg_{}_choosingNum_草莓_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("🔔",
                                 callback_data=str(
                                     'bg_{}_choosingNum_铃铛_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("🍉",
                                 callback_data=str(
                                     'bg_{}_choosingNum_西瓜_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("⭐️",
                                 callback_data=str(
                                     'bg_{}_choosingNum_星星_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("💎",
                                 callback_data=str(
                                     'bg_{}_choosingNum_钻石_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("🏆",
                                 callback_data=str(
                                     'bg_{}_choosingNum_奖杯_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("结算游戏",
                                 callback_data=str(
                                     'bg_{}_endGame_{}_None_None_None'.format(group_id, timestamp))),
        ]]


def get_random_num():
    return random.randint(1, 100)


def num_to_text(num):
    num = int(num)
    if 1 <= num <= 14:
        return "苹果"
    elif 15 <= num <= 22:
        return "苹果2"
    elif 23 <= num <= 32:
        return "橙子"
    elif 33 <= num <= 37:
        return "橙子2"
    elif 38 <= num <= 47:
        return "草莓"
    elif 48 <= num <= 52:
        return "草莓2"
    elif 53 <= num <= 62:
        return "铃铛"
    elif 63 <= num <= 67:
        return "铃铛2"
    elif 68 <= num <= 74:
        return "西瓜"
    elif 75 <= num <= 77:
        return "西瓜2"
    elif 78 <= num <= 84:
        return "星星"
    elif 85 <= num <= 87:
        return "星星2"
    elif 88 <= num <= 94:
        return "钻石"
    elif 95 <= num <= 97:
        return "钻石2"
    elif 98 <= num <= 99:
        return "奖杯"
    elif num == 100:
        return "奖杯2"


def num_to_symbol(num):
    num = int(num)
    if 1 <= num <= 14:
        return "🍎"
    elif 15 <= num <= 22:
        return "🍎x2"
    elif 23 <= num <= 32:
        return "🍊"
    elif 33 <= num <= 37:
        return "🍊x2"
    elif 38 <= num <= 47:
        return "🍓"
    elif 48 <= num <= 52:
        return "🍓x2"
    elif 53 <= num <= 62:
        return "🔔"
    elif 63 <= num <= 67:
        return "🔔x2"
    elif 68 <= num <= 74:
        return "🍉"
    elif 75 <= num <= 77:
        return "🍉x2"
    elif 78 <= num <= 84:
        return "⭐"
    elif 85 <= num <= 87:
        return "⭐x2"
    elif 88 <= num <= 94:
        return "💎"
    elif 95 <= num <= 97:
        return "💎x2"
    elif 98 <= num <= 99:
        return "🏆"
    elif num == 100:
        return "🏆x2"


def num_to_rate(num):
    num = int(num)
    if 1 <= num <= 14:
        return 2
    elif 15 <= num <= 22:
        return 5
    elif 23 <= num <= 32:
        return 3
    elif 33 <= num <= 37:
        return 6
    elif 38 <= num <= 47:
        return 4
    elif 48 <= num <= 52:
        return 8
    elif 53 <= num <= 62:
        return 4
    elif 63 <= num <= 67:
        return 8
    elif 68 <= num <= 74:
        return 5
    elif 75 <= num <= 77:
        return 10
    elif 78 <= num <= 84:
        return 6
    elif 85 <= num <= 87:
        return 12
    elif 88 <= num <= 94:
        return 9
    elif 95 <= num <= 97:
        return 18
    elif 98 <= num <= 99:
        return 24
    elif num == 100:
        return 48


def text_to_symbol(text):
    if text == "苹果":
        return "🍎"
    elif text == "橙子":
        return "🍊"
    elif text == "草莓":
        return "🍓"
    elif text == "铃铛":
        return "🔔"
    elif text == "西瓜":
        return "🍉"
    elif text == "星星":
        return "⭐️"
    elif text == "钻石":
        return "💎"
    elif text == "奖杯":
        return "🏆"


def bg(update, context):
    try:
        message = utils.ana_message(update)
        if utils.is_user_in_black_list(message.user_id):
            update.message.reply_text("黑名单用户，无权操作")
            return
        chat_type = message.group_type
        user_id = message.user_id
        chat_id = message.group_id
        # 限制为群组
        if chat_type != "supergroup" and chat_type != "group":
            update.message.reply_text("此命令只有在群组中有效")
            return
        utils.get_user_balance_update(user_id, update)
        timestamp = utils.get_timestamp()
        r = connector.get_connection(13)
        if utils.new_game_lock_existed(chat_id):
            bot.sendMessage(
                chat_id=chat_id,
                text="开局受限\n每个会话窗口每隔90s才可新建一局\n上一局游戏完成结算后立即解锁新局"
            )
            return
        r.hmset("{}_{}".format(chat_id, timestamp), {
            "status": "created",
            "num": get_random_num(),
            "participant": utils.list_to_string([])
        })
        utils.set_new_game_lock(chat_id)
        reply_markup = InlineKeyboardMarkup(make_keyboard1(chat_id, timestamp))
        bot.sendMessage(
            chat_id=utils.ana_message(update).group_id,
            text="*水果机* 正在投注\n\n{}".format(utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
        # print("群组ID：{}|游戏ID：{}".format(message.group_id, timestamp))
    except Exception as e:
        print(e)


def start_game(update):
    print("start_game")
    r = connector.get_connection(13)
    message = utils.ana_message(update)
    game_name, group_id, action, game_id, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
    status, num, participant = r.hmget("{}_{}".format(message.group_id, game_id), ['status', 'num', 'participant'])
    print("获取到 Redis 中的数据", status, num, participant)
    participant = utils.string_to_list(participant)
    if status == "choosing":
        # utils.show_msg(update, "游戏已经开始，请选择数字", True)
        rest_time = r.ttl("{}_{}_time_limit".format(message.group_id, game_id))
        if "-" in str(rest_time):
            rest_time = 0
        print("由于更新问题，重新向未更新用户推送键盘")
        query = update.callback_query
        reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
        query.edit_message_text(
            text="*水果机* 选择中\n\n"
                 "此游戏结算后显示统一下注记录\n\n"
                 "{}\n\n{}".format(generate_recent_history(message.group_id), utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
        return
    if status == "end":
        utils.show_msg(update, "游戏已经结束", True)
        return
    if len(participant) == 0:
        utils.show_msg(update, "目前没有用户下注，请等待用户下注后开始游戏", True)
        return
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return
    if r.exists("{}_{}_start_lock".format(message.group_id, game_id)):
        utils.show_msg(update, "所有用户无操作6s后才能开始游戏", True)
        return
    r.hset("{}_{}".format(message.group_id, game_id), "status", "choosing")
    # 设置游戏选择时间
    r.set("{}_{}_time_limit".format(message.group_id, game_id), "", ex=60)
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
    query.edit_message_text(
        text="*水果机* 选择中\n\n"
             "此游戏结算后显示统一下注记录\n\n"
             "{}\n\n{}".format(generate_recent_history(message.group_id), utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )


def add_money(update, context):
    print("add")
    message = utils.ana_message(update)
    balance = utils.get_user_balance_update(message.user_id, update)
    if balance <= 0:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(13)
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    print(status)
    if status != "created":
        utils.show_msg(update, "下注已结束，无法下注！", True)
        return
    total_amount, add_money_times, choose_times = 0, 0, 0
    print("游戏ID" + str(query_data.arg2))
    print("下注数据是否存在：" + str(r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id))))
    if r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id)):
        print("数据库读取下注数据")
        print(r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"]))
        total_amount, add_money_times, choose_times = r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"])
    else:
        print("第一次下注，添加数据")
        r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
            "name": message.name,
            "total_amount": 0,
            "add_money_times": 0,
            "choose_times": 0,
            "choose_num": -1,
        })
    total_amount = int(total_amount)
    add_money_times = int(add_money_times)
    if add_money_times >= 5:
        utils.show_msg(update, "加注次数超过5次，此次加注失败！", True)
        return
    query_data = utils.ana_query_data(update)
    add_amount = query_data.arg1
    print("用户选择的下注金额：" + add_amount)
    if add_amount == "1.0":
        add_amount = balance
    elif "." in add_amount:
        add_amount = balance * float(add_amount)
        print("百分比折算后的下注金额：" + str(add_amount))
    print("之前下注总金额：" + str(total_amount))
    print("此次下注金额：" + str(add_amount))
    add_amount = int(add_amount)
    if balance < add_amount:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    utils.reduce_user_balance(message.user_id, add_amount)
    total_amount += add_amount
    print("当前总下注金额：" + str(total_amount))
    add_money_times += 1
    r.set("{}_{}_start_lock".format(message.group_id, query_data.arg2), "", ex=6)
    r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
        "total_amount": total_amount,
        "add_money_times": add_money_times,
        "choose_times": 0
    })
    participant = utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), 'participant'))
    if message.user_id not in participant:
        participant.append(message.user_id)
    r.hset("{}_{}".format(message.group_id, query_data.arg2), "participant", utils.list_to_string(participant))
    utils.show_msg(update, "下注成功\n您当前下注总额：${}\n请在无人跟注6s后点击开始游戏!".format(utils.balance_format(total_amount)), True)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    #   更新显示文字
    reply_markup = InlineKeyboardMarkup(make_keyboard1(message.group_id, query_data.arg2))
    query = update.callback_query
    query.edit_message_text(
        text="*水果机* 正在投注\n\n玩家列表：\n{}\n\n{}".format(generate_add_money_user(message.group_id, query_data.arg2),
                                                    utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)


def choosing(update, context):
    print("choosing")
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(13)
    print("选择时间限制，键：" + str(r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2))))
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != "choosing":
        utils.show_msg(update, "此局游戏已经结算！", True)
        return
    if not r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        utils.show_msg(update, "选择阶段已经结束，请尽快结算本局游戏", True)
        return
    if int(r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times")) >= 10:
        utils.show_msg(update, "最多押注10次，此次押注失败", True)
        return
    choose_num = query_data.arg1
    print("用户选择的为:" + choose_num)
    # 更新用户选择的数字

    print("更新用户选择的数字")
    user_choose = r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_num")
    print("Redis 中，用户的选择{}".format(user_choose))

    if len(str(user_choose)) != 2:
        print("Not -1")
        user_choose = ast.literal_eval(user_choose)
        print(user_choose)
        user_choose.update({choose_num: user_choose.get(choose_num, 0) + 1})
    else:
        print("Is -1")
        user_choose = {choose_num: 1}
    print(str(user_choose))
    # 刷写 Redis
    r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_num", str(user_choose))

    choose_times = int(r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times"))
    choose_times += 1
    print("更新用户选择次数")
    r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times", choose_times)
    # 设置冷却时间
    print("设置结算游戏锁定")
    r.set("{}_{}_end_lock".format(message.group_id, query_data.arg2), "", ex=6)

    text = "您当前的押注为：\n\n"
    for name, weight in user_choose.items():
        text = text + text_to_symbol(name) + " " + "x" + str(weight) + "\n"
    text = text + "\n" + "结算时，将根据您押中水果的下注比例结算奖励"
    print(text)
    utils.show_msg(update, text, True)

    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, query_data.arg2))
    # query.edit_message_text(
    #     text="*水果机* 选择中\({}\)\n\n"
    #          "此游戏结算后显示统一下注记录\n\n"
    #          "{}\n\n"
    #          "{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, query_data.arg2)),
    #                      generate_recent_history(message.group_id), utils.get_random_ad()),
    #     reply_markup=reply_markup,
    #     parse_mode="MarkdownV2",
    #     disable_web_page_preview=True
    # )


def end_game(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(13)
    if not utils.end_game_check(update, query_data.arg1, 13):
        return
    print("开始进入结算流程")
    # 将游戏设置为结束状态
    r.hset("{}_{}".format(message.group_id, query_data.arg1), "status", "end")
    r.rpush("{}_history".format(message.group_id),
            int(r.hget("{}_{}".format(message.group_id, query_data.arg1), "num")))
    print("开始计算用户余额")
    rst_message_id = update.callback_query.message.message_id
    show_text, reply_text, is_reply = calculate(message.group_id, query_data.arg1)
    keyboard = [[
        InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
        InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    print(show_text)
    print("准备返回数据")
    query = update.callback_query
    query.edit_message_text(
        text=show_text + "\n\n{}".format(utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    utils.del_new_game_lock(message.group_id)
    if is_reply:
        try:
            bot.sendMessage(
                chat_id=message.group_id,
                text="🎉🎉🎉祝贺本场轮盘 大赢家\n\n" + reply_text,
                reply_to_message_id=rst_message_id,
                parse_mode="Markdown"
            )
        except Exception as e:
            print(e)


def ana_amount(total_amount, user_choose, rst):
    total_weight = 0
    target_weight = 0
    for fruit, weight in user_choose.items():
        total_weight += weight
        if fruit == rst:
            target_weight = weight
    target_amount = int(float(total_amount) * (float(target_weight) / float(total_weight)))
    return target_amount


def calculate(group_id, timestamp):
    print("calculate")
    r = connector.get_connection(13)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, timestamp), "participant"))
    num = int(r.hget("{}_{}".format(group_id, timestamp), "num"))
    print("系统生成的数字为：" + str(num))
    text = ""
    reply_text = ""
    rst_text = num_to_text(num)
    rst_rate = num_to_rate(num)
    rst_text2 = rst_text[:2]
    for user in participant:
        user_choose, total_amount, name = r.hmget("{}_{}_{}".format(group_id, timestamp, user),
                                                  ["choose_num", "total_amount", "name"])
        total_amount = int(total_amount)

        if len(str(user_choose)) != 2:
            user_choose = ast.literal_eval(user_choose)
            if rst_text2 in user_choose.keys():
                print("此用户中奖")
                total_amount = ana_amount(total_amount, user_choose, rst_text2)
                print("水果正确")
                bonus = total_amount * rst_rate
                print('奖金为：${}'.format(utils.balance_format(bonus)))
                utils.add_user_balance(user, bonus)
                text += "【[{}](https://t.me/gameyyds)】{}：{} ${} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                    text_to_symbol(rst_text2),
                                                                                    utils.balance_format(total_amount),
                                                                                    utils.balance_format(bonus))
                print(text)
                utils.increase_win_amount(user, bonus)
                if bonus >= 100000000:
                    reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：赢 +${}\n".format(
                        utils.get_title(user), name, utils.balance_format(bonus))
            else:
                text += "【[{}](https://t.me/gameyyds)】{}：没有押中 *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                                    utils.balance_format(total_amount))
        else:
            print("该用户未做出选择，返还余额")
            utils.add_user_balance(user, total_amount)
            text += "【[{}](https://t.me/gameyyds)】{}：*退回* ${}\n".format(utils.get_title(user), name,
                                                                        utils.balance_format(total_amount))

    show_text = "*水果机* 本场结算\n\n" \
                "开奖结果: {} \({}倍\)\n\n" \
                "玩家列表：\n" \
                "{}\n" \
                "{}".format(num_to_symbol(num), rst_rate, text, generate_recent_history(group_id))
    print(show_text)
    if len(reply_text) > 0:
        return show_text, reply_text, True
    else:
        return show_text, "", False


def generate_add_money_user(group_id, game_id):
    text = ""
    r = connector.get_connection(13)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, amount = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "total_amount"])
        text += "【[{}](https://t.me/gameyyds)】{}：${}".format(utils.get_title(user), name,
                                                             utils.balance_format(amount)) + "\n"
    return text[:-1]


def generate_choosing_user(group_id, game_id):
    text = ""
    r = connector.get_connection(13)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, choose_num = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "choose_num"])
        if choose_num == '-1':
            text += "【[{}](https://t.me/gameyyds)】{}：🕒 未选择\n".format(utils.get_title(user), name)
        else:
            text += "【[{}](https://t.me/gameyyds)】{}：✅ {}\n".format(utils.get_title(user), name, choose_num)
    return text


def generate_recent_history(group_id):
    text = ""
    r = connector.get_connection(13)
    len_history = int(r.llen("{}_history".format(group_id)))
    max_length = 0
    if len_history > 10:
        len_history = 10
        max_length = -len_history
    text += "最近{}场历史：\n".format(len_history)
    history = r.lrange("{}_history".format(group_id), max_length, -1)
    for rst in history:
        text += text_to_symbol(num_to_text(rst)[:2]) + " "
    print(text)
    return text[:-1]


def process(update, context):
    query = update.callback_query
    try:
        game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
        if action == "startGame":
            start_game(update)
        elif action == "addMoney":
            add_money(update, context)
        elif action == "choosingNum":
            choosing(update, context)
        elif action == "endGame":
            end_game(update, context)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")


bg_handler = CommandHandler('bg', bg)
