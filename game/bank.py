import re

import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random

bot = telegram.Bot(token=TOKEN)

# 每日利率
RATE = 0.002

# 取款手续费
FEE = 0


def ck(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if message.group_type != "private":
        update.message.reply_text("存款命令只能私聊使用")
        return
    user_input = update.message.text
    rst = re.match("\/ck\s(\d+)$", user_input)
    if rst is None:
        update.message.reply_text(
            "您的当前存款为：${}\n存款指令：/ck 金额\n存款金额 >= $100,0000".format(utils.balance_format(get_user_ck(message.user_id))))
        return
    amount = int(rst.group(1))
    balance = utils.get_user_balance(message.user_id)
    if amount > balance:
        update.message.reply_text("存款金额不能大于当前余额，当前余额：${}".format(utils.balance_format(balance)))
        return
    if amount < 1000000:
        update.message.reply_text("单次存款金额必须 >= $100,0000")
        return
    else:
        utils.reduce_user_balance(message.user_id, amount)
        add_user_ck(message.user_id, amount)
        update.message.reply_text(
            "存款成功，当前存款：${}，当前每日利率：{}".format(utils.balance_format(get_user_ck(message.user_id)), RATE))
        return


def qk(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if message.group_type != "private":
        update.message.reply_text("取款命令只能私聊使用")
        return
    user_input = update.message.text
    rst = re.match("\/qk\s(\d+)$", user_input)
    if rst is None:
        update.message.reply_text(
            "您的当前存款为：${}\n取款指令：/qk 金额\n金额 >= $100,0000\n每次取款手续费：{}%".format(
                utils.balance_format(get_user_ck(message.user_id)), int(FEE * 100)))
        return
    amount = int(rst.group(1))
    user_ck = get_user_ck(message.user_id)
    if user_ck < amount:
        update.message.reply_text("取款金额不能大于当前存款金额，当前存款：${}".format(utils.balance_format(user_ck)))
        return
    if amount < 1000000:
        update.message.reply_text("每次取款不能少于$100,0000，此次取款金额为：${}".format(utils.balance_format(amount)))
        return
    reduce_user_ck(message.user_id, amount)
    utils.add_user_balance(message.user_id, int(float(amount) * (1.0 - FEE)))
    update.message.reply_text(
        "取款成功\n"
        "此次取款：${}\n"
        "手续费：${}\n"
        "当前存款：${}".format(
            utils.balance_format(amount),
            utils.balance_format(int(float(amount) * FEE)),
            utils.balance_format(get_user_ck(message.user_id)),
        )
    )


def get_user_ck(user_id):
    r = connector.get_connection(11)
    if r.exists(user_id):
        return int(r.get(user_id))
    else:
        r.set(user_id, 0)
        return 0


def add_user_ck(user_id, amount):
    r = connector.get_connection(11)
    old_ck = get_user_ck(user_id)
    new_ck = old_ck + int(amount)
    r.set(user_id, new_ck)


def reduce_user_ck(user_id, amount):
    r = connector.get_connection(11)
    old_ck = get_user_ck(user_id)
    new_ck = old_ck - int(amount)
    r.set(user_id, new_ck)


ck_handler = CommandHandler("ck", ck)
qk_handler = CommandHandler("qk", qk)
