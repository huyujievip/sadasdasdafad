import ast

import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random

bot = telegram.Bot(token=TOKEN)


def make_keyboard1(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("1%",
                                 callback_data=str(
                                     'bk_{}_addMoney_0.01_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5%",
                                 callback_data=str(
                                     'bk_{}_addMoney_0.05_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20%",
                                 callback_data=str(
                                     'bk_{}_addMoney_0.2_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50%",
                                 callback_data=str(
                                     'bk_{}_addMoney_0.5_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("80%",
                                 callback_data=str(
                                     'bk_{}_addMoney_0.8_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("梭哈",
                                 callback_data=str(
                                     'bk_{}_addMoney_1.0_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2W", callback_data=str(
                'bk_{}_addMoney_20000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5W", callback_data=str(
                'bk_{}_addMoney_50000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20W", callback_data=str(
                'bk_{}_addMoney_200000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50W", callback_data=str(
                'bk_{}_addMoney_500000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200W", callback_data=str(
                'bk_{}_addMoney_2000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500W", callback_data=str(
                'bk_{}_addMoney_5000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2KW", callback_data=str(
                'bk_{}_addMoney_20000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KW", callback_data=str(
                'bk_{}_addMoney_50000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2E", callback_data=str(
                'bk_{}_addMoney_200000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5E", callback_data=str(
                'bk_{}_addMoney_500000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20E", callback_data=str(
                'bk_{}_addMoney_2000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50E", callback_data=str(
                'bk_{}_addMoney_5000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("200E", callback_data=str(
                'bk_{}_addMoney_20000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500E", callback_data=str(
                'bk_{}_addMoney_50000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KE", callback_data=str(
                'bk_{}_addMoney_200000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KE", callback_data=str(
                'bk_{}_addMoney_500000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2WE", callback_data=str(
                'bk_{}_addMoney_2000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5WE", callback_data=str(
                'bk_{}_addMoney_5000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("20WE", callback_data=str(
                'bk_{}_addMoney_20000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50WE", callback_data=str(
                'bk_{}_addMoney_50000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200WE", callback_data=str(
                'bk_{}_addMoney_200000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500WE", callback_data=str(
                'bk_{}_addMoney_500000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KWE", callback_data=str(
                'bk_{}_addMoney_2000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5kWE", callback_data=str(
                'bk_{}_addMoney_5000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("1EE", callback_data=str(
                'bk_{}_addMoney_10000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("3EE", callback_data=str(
                'bk_{}_addMoney_30000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5EE", callback_data=str(
                'bk_{}_addMoney_50000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("10EE", callback_data=str(
                'bk_{}_addMoney_100000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50EE", callback_data=str(
                'bk_{}_addMoney_500000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("99EE", callback_data=str(
                'bk_{}_addMoney_990000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("▶️开始游戏",
                                 callback_data=str(
                                     'bk_{}_startGame_{}_None_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
            InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
        ]
    ]


def make_keyboard2(group_id, timestamp):
    return [[
        InlineKeyboardButton("📥要牌",
                             callback_data=str(
                                 'bk_{}_choosingNum_要牌_{}_None_None'.format(group_id, timestamp))),
        InlineKeyboardButton("🔔停牌",
                             callback_data=str(
                                 'bk_{}_choosingNum_停牌_{}_None_None'.format(group_id, timestamp))),
    ], [
        InlineKeyboardButton("结算游戏",
                             callback_data=str(
                                 'bk_{}_endGame_{}_None_None_None'.format(group_id, timestamp))),
    ]]


def a_in_puke_list(puke_list):
    rst = False
    if 1 in puke_list:
        return True
    elif 14 in puke_list:
        return True
    elif 27 in puke_list:
        return True
    elif 40 in puke_list:
        return True
    return rst


def ten_in_puke_list(puke_list):
    rst = False
    if 10 in puke_list:
        return True
    elif 11 in puke_list:
        return True
    elif 12 in puke_list:
        return True
    elif 13 in puke_list:
        return True
    elif 23 in puke_list:
        return True
    elif 24 in puke_list:
        return True
    elif 25 in puke_list:
        return True
    elif 26 in puke_list:
        return True
    elif 36 in puke_list:
        return True
    elif 37 in puke_list:
        return True
    elif 38 in puke_list:
        return True
    elif 39 in puke_list:
        return True
    elif 49 in puke_list:
        return True
    elif 50 in puke_list:
        return True
    elif 51 in puke_list:
        return True
    elif 52 in puke_list:
        return True
    return rst


def generate_puke(group_id, game_id, puke_list):
    flag = False
    if a_in_puke_list(puke_list):
        flag = True
    r = connector.get_connection(15)
    if not r.exists("{}_{}_puke_history".format(group_id, game_id)):
        if flag:
            while True:
                puke = random.randint(1, 52)
                if puke in [13, 26, 39, 52]:
                    continue
                else:
                    break
        else:
            puke = random.randint(1, 52)
        r.set("{}_{}_puke_history".format(group_id, game_id), str([puke]))
        return puke
    else:
        puke_list = ast.literal_eval(r.get("{}_{}_puke_history".format(group_id, game_id)))
        if len(puke_list) == 52:
            return 0
        while True:
            if flag:
                while True:
                    puke = random.randint(1, 52)
                    if puke in [13, 26, 39, 52]:
                        continue
                    else:
                        break
            else:
                puke = random.randint(1, 52)
            if puke in puke_list and len(puke_list) < 48:
                continue
            else:
                puke_list.append(puke)
                r.set("{}_{}_puke_history".format(group_id, game_id), str(puke_list))
                break
        return puke


def num_to_text(num):
    num = int(num) % 13
    alist = ["K", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", ""]
    return alist[num]


def num_to_point_zhuang(num):
    num = int(num) % 13
    alist = [10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 0]
    return alist[num]


def num_to_point(num):
    num = int(num) % 13
    alist = [10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 0]
    return alist[num]


def generate_zhuang(group_id, game_id):
    zhuang_list = []
    while calculate_zhuang_point(zhuang_list) < 17:
        zhuang_list.append(generate_puke(group_id, game_id, zhuang_list))
    return str(zhuang_list)


def generate_xian(group_id, game_id):
    xian_list = []
    xian_list.append(generate_puke(group_id, game_id, xian_list))
    xian_list.append(generate_puke(group_id, game_id, xian_list))
    return xian_list


def calculate_zhuang_point(zhuang_list):
    rst = 0
    for i in zhuang_list:
        rst += num_to_point_zhuang(i)
    return rst


def process(update, context):
    query = update.callback_query
    try:
        game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
        if action == "startGame":
            start_game(update)
        elif action == "addMoney":
            add_money(update, context)
        elif action == "choosingNum":
            choosing(update, context)
        elif action == "endGame":
            end_game(update, context)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")


def two_random_nums():
    rst = []
    for i in range(2):
        rst.append(random.randint(1, 13))
    return utils.list_to_string(rst)


def bk(update, context):
    try:
        message = utils.ana_message(update)
        if utils.is_user_in_black_list(message.user_id):
            update.message.reply_text("黑名单用户，无权操作")
            return
        chat_type = message.group_type
        user_id = message.user_id
        chat_id = message.group_id
        # 限制为群组
        if chat_type != "supergroup" and chat_type != "group":
            update.message.reply_text("此命令只有在群组中有效")
            return
        utils.get_user_balance_update(user_id, update)
        timestamp = utils.get_timestamp()
        r = connector.get_connection(15)
        if utils.new_game_lock_existed(chat_id):
            bot.sendMessage(
                chat_id=chat_id,
                text="开局受限\n每个会话窗口每隔90s才可新建一局\n上一局游戏完成结算后立即解锁新局"
            )
            return
        r.hmset("{}_{}".format(chat_id, timestamp), {
            "status": "created",
            "num": generate_zhuang(chat_id, timestamp),
            "participant": utils.list_to_string([])
        })
        utils.set_new_game_lock(chat_id)
        reply_markup = InlineKeyboardMarkup(make_keyboard1(chat_id, timestamp))
        bot.sendMessage(
            chat_id=utils.ana_message(update).group_id,
            text="*21点* 正在投注\n\n{}".format(utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
    except Exception as e:
        print(e)


def start_game(update):
    print("start_game")
    r = connector.get_connection(15)
    message = utils.ana_message(update)
    game_name, group_id, action, game_id, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
    status, num, participant = r.hmget("{}_{}".format(message.group_id, game_id), ['status', 'num', 'participant'])
    print("获取到 Redis 中的数据", status, num, participant)
    participant = utils.string_to_list(participant)
    if status == "choosing":
        rest_time = r.ttl("{}_{}_time_limit".format(message.group_id, game_id))
        if "-" in str(rest_time):
            rest_time = 0
        print("由于更新问题，重新向未更新用户推送键盘")
        query = update.callback_query
        reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
        query.edit_message_text(
            text="*21点* 选择中\({}\)\n\n"
                 "庄家：🃏{}\n\n"
                 "玩家列表：\n"
                 "{}\n"
                 "{}\n\n"
                 "{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, game_id)),
                             num_to_text(utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "num"))[0]),
                             generate_choosing_user(message.group_id, game_id),
                             generate_recent_history(message.group_id),
                             utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
        return
    if status == "end":
        utils.show_msg(update, "游戏已经结束", True)
        return
    if len(participant) == 0:
        utils.show_msg(update, "目前没有用户下注，请等待用户下注后开始游戏", True)
        return
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return
    if r.exists("{}_{}_start_lock".format(message.group_id, game_id)):
        utils.show_msg(update, "所有用户无操作6s后才能开始游戏", True)
        return
    r.hset("{}_{}".format(message.group_id, game_id), "status", "choosing")
    # 给每位用户初始化手牌
    for user in participant:
        choose_num = generate_xian(group_id, game_id)
        r.hset("{}_{}_{}".format(group_id, game_id, user), "choose_num", utils.list_to_string(choose_num))
        r.hset("{}_{}_{}".format(group_id, game_id, user), "choosing_status", str(1))
    # 设置游戏选择时间
    r.set("{}_{}_time_limit".format(message.group_id, game_id), "", ex=60)
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
    query.edit_message_text(
        text="*21点* 选择中\({}\)\n\n"
             "庄家：🃏{}\n\n"
             "玩家列表：\n"
             "{}\n"
             "{}\n\n"
             "{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, game_id)),
                         num_to_text(utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "num"))[0]),
                         generate_choosing_user(message.group_id, game_id),
                         generate_recent_history(message.group_id),
                         utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )


def add_money(update, context):
    message = utils.ana_message(update)
    balance = utils.get_user_balance_update(message.user_id, update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(15)
    if balance <= 0:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    participant = utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), 'participant'))
    if len(participant) > 15:
        utils.show_msg(update, "此轮游戏人数已达上限，无法下注", True)
        return
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != "created":
        utils.show_msg(update, "下注已结束，无法下注！", True)
        return
    total_amount, add_money_times, choose_times = 0, 0, 0
    print("游戏ID" + str(query_data.arg2))
    print("下注数据是否存在：" + str(r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id))))
    if r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id)):
        print("数据库读取下注数据")
        print(r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"]))
        total_amount, add_money_times, choose_times = r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"])
    else:
        print("第一次下注，添加数据")
        r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
            "name": message.name,
            "total_amount": 0,
            "add_money_times": 0,
            "choose_times": 0,
            "choose_num": -1,
        })
    total_amount = int(total_amount)
    add_money_times = int(add_money_times)
    if add_money_times >= 5:
        utils.show_msg(update, "加注次数超过5次，此次加注失败！", True)
        return
    query_data = utils.ana_query_data(update)
    add_amount = query_data.arg1
    print("用户选择的下注金额：" + add_amount)
    if add_amount == "1.0":
        add_amount = balance
    elif "." in add_amount:
        add_amount = balance * float(add_amount)
        print("百分比折算后的下注金额：" + str(add_amount))
    print("之前下注总金额：" + str(total_amount))
    print("此次下注金额：" + str(add_amount))
    add_amount = int(add_amount)
    if balance < add_amount:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    utils.reduce_user_balance(message.user_id, add_amount)
    total_amount += add_amount
    print("当前总下注金额：" + str(total_amount))
    add_money_times += 1
    r.set("{}_{}_start_lock".format(message.group_id, query_data.arg2), "", ex=6)
    r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
        "total_amount": total_amount,
        "add_money_times": add_money_times,
        "choose_times": 0
    })
    participant = utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), 'participant'))
    if message.user_id not in participant:
        participant.append(message.user_id)
    r.hset("{}_{}".format(message.group_id, query_data.arg2), "participant", utils.list_to_string(participant))
    utils.show_msg(update, "下注成功\n您当前下注总额：${}\n请在无人跟注6s后点击开始游戏!".format(utils.balance_format(total_amount)), True)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    #   更新显示文字
    reply_markup = InlineKeyboardMarkup(make_keyboard1(message.group_id, query_data.arg2))
    query = update.callback_query
    query.edit_message_text(
        text="*21点* 正在投注\n\n玩家列表：\n" + generate_add_money_user(message.group_id, query_data.arg2) + "\n\n{}".format(
            utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)


def choosing(update, context):
    print("choosing")
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(15)
    print("选择时间限制，键：" + str(r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2))))
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != "choosing":
        utils.show_msg(update, "此局游戏已经结算！", True)
        return
    if not r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        utils.show_msg(update, "选择阶段已经结束，请尽快结算本局游戏", True)
        return
    if int(r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times")) >= 6:
        utils.show_msg(update, "选择最多只能要牌5次，此次更改失败", True)
        return
    choose_num = query_data.arg1
    print("用户选择的为:" + choose_num)

    if str(choose_num) == "要牌":
        user_status = r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choosing_status")
        if str(user_status) == "0":
            utils.show_msg(update, "您已停牌，无法继续要牌", True)
            return
        puke_list = utils.string_to_list(
            r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_num"))
        if is_boom(puke_list):
            utils.show_msg(update, "您已爆牌，无法继续要牌", True)
            return
        else:
            puke = generate_puke(message.group_id, query_data.arg2, puke_list)
            if puke == 0:
                utils.show_msg(update, "此轮扑克发放完毕，无法获得新的手牌", True)
                return
            puke_list.append(puke)
            if check_point(puke_list) > 21:
                r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choosing_status", "0")
            utils.show_msg(update, "您获取新手牌：🃏 {}".format(num_to_text(puke)), True)
            print("更新用户选择的数字")
            r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_num", str(puke_list))
        choose_times = int(
            r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times"))
        choose_times += 1
        print("更新用户选择次数")
        r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times", choose_times)
        # 设置冷却时间
        print("设置结算游戏锁定")
        r.set("{}_{}_end_lock".format(message.group_id, query_data.arg2), "", ex=6)
    if str(choose_num) == "停牌":
        r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choosing_status", "0")
        utils.show_msg(update, "您已停牌，无法再次获取新的手牌", True)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, query_data.arg2))
    query.edit_message_text(
        text="*21点* 选择中\({}\)\n\n"
             "庄家：🃏{}\n\n"
             "玩家列表：\n"
             "{}\n"
             "{}\n\n"
             "{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, query_data.arg2)),
                         num_to_text(
                             utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), "num"))[0]),
                         generate_choosing_user(message.group_id, query_data.arg2),
                         generate_recent_history(message.group_id),
                         utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)


def end_game(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(15)
    stop_user = 0
    status, num, participant = r.hmget("{}_{}".format(message.group_id, query_data.arg1),
                                       ['status', 'num', 'participant'])
    print("获取到 Redis 中的数据", status, num, participant)
    participant = utils.string_to_list(participant)
    if status != "choosing":
        utils.show_msg(update, "此局游戏已经结算！由于系统限制，显示文字刷新失败，但是结算金额已经到账！", True)
        return False
    print("开始判断用户是否参与了此局游戏")
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return False
    # print("判断是否存在冷却时间")
    # if r.exists("{}_{}_end_lock".format(message.group_id, query_data.arg1)):
    #     print("当前处在冷却时间")
    #     utils.show_msg(update, "请等待无用户操作6s后结算游戏", True)
    #     return False
    print("开始遍历用户")
    for user in participant:
        print("判断用户是否做出选择")
        if r.hget("{}_{}_{}".format(message.group_id, query_data.arg1, user), "choosing_status") == "0":
            print("此用户已经停牌")
            stop_user += 1
    end_time_limit = int(r.ttl("{}_{}_time_limit".format(message.group_id, query_data.arg1)))
    print("判断是否满足终止游戏条件")
    if stop_user != len(participant) and end_time_limit > 0:
        utils.show_msg(update, "当前还有用户没有停牌，请等待所有用户停牌或者游戏结束后结算", True)
        return
    print("开始进入结算流程")
    # 将游戏设置为结束状态
    r.hset("{}_{}".format(message.group_id, query_data.arg1), "status", "end")
    print("开始计算用户余额")
    rst_message_id = update.callback_query.message.message_id
    show_text, reply_text, is_reply = calculate(message.group_id, query_data.arg1)
    keyboard = [[
        InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
        InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    print(show_text)
    print("准备返回数据")
    query = update.callback_query
    query.edit_message_text(
        text=show_text + "\n{}".format(utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    utils.del_new_game_lock(message.group_id)
    if is_reply:
        try:
            bot.sendMessage(
                chat_id=message.group_id,
                text="🎉🎉🎉祝贺本场*21点* 大赢家\n\n" + reply_text,
                reply_to_message_id=rst_message_id,
                parse_mode="Markdown"
            )
        except Exception as e:
            print(e)


def calculate(group_id, timestamp):
    print("calculate")
    r = connector.get_connection(15)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, timestamp), "participant"))
    zhuang_list = utils.string_to_list(r.hget("{}_{}".format(group_id, timestamp), "num"))
    print("庄家的手牌为：" + str(zhuang_list))
    zhuang_point = calculate_zhuang_point(zhuang_list)
    text = ""
    rst = ""
    reply_text = ""
    win_count = 0
    lose_count = 0
    win_amount = 0
    lose_amount = 0
    for user in participant:
        choose_num, total_amount, name = r.hmget("{}_{}_{}".format(group_id, timestamp, user),
                                                 ["choose_num", "total_amount", "name"])
        puke_list = utils.string_to_list(choose_num)
        total_amount = int(total_amount)
        bonus = 0
        # 庄家炸了，用户没有炸
        if is_boom(zhuang_list) and not is_boom(puke_list):
            if is_bk(puke_list):
                bonus = int(total_amount * 2.5)
            else:
                bonus = total_amount * 1.99
            text += "【[{}](https://t.me/gameyyds)】{}：{} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                            generate_choose_status(puke_list),
                                                                            utils.balance_format(bonus))
            utils.add_user_balance(user, bonus)
            utils.increase_win_amount(user, bonus)
            lose_count += 1
            lose_amount += (bonus - total_amount)
            if bonus >= 100000000:
                reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：赢 +${}\n".format(
                    utils.get_title(user), name, utils.balance_format(bonus))
        # 用户炸了，庄家没有炸
        elif is_boom(puke_list) and not is_boom(zhuang_list):
            text += "【[{}](https://t.me/gameyyds)】{}：{} *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                              generate_choose_status(puke_list),
                                                                              utils.balance_format(total_amount))
            win_count += 1
            win_amount += total_amount
        # 都炸了
        elif is_boom(puke_list) and is_boom(zhuang_list):
            text += "【[{}](https://t.me/gameyyds)】{}：{} *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                              generate_choose_status(puke_list),
                                                                              utils.balance_format(total_amount))
            win_count += 1
            win_amount += total_amount
        else:
            # 玩家是黑杰克
            if is_bk(puke_list):
                if is_bk(zhuang_list):
                    # 平局，退回
                    bonus = total_amount
                    text += "【[{}](https://t.me/gameyyds)】{}：{} *平局* 退回${}\n".format(utils.get_title(user), name,
                                                                                     generate_choose_status(puke_list),
                                                                                     utils.balance_format(bonus))
                    utils.add_user_balance(user, bonus)
                else:
                    bonus = int(total_amount * 2.5)
                    text += "【[{}](https://t.me/gameyyds)】{}：{} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                    generate_choose_status(puke_list),
                                                                                    utils.balance_format(bonus))
                    utils.add_user_balance(user, bonus)
                    utils.increase_win_amount(user, bonus)
                    lose_count += 1
                    lose_amount += (bonus - total_amount)
                    if bonus >= 100000000:
                        reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：赢 +${}\n".format(
                            utils.get_title(user), name, utils.balance_format(bonus))
            # 玩家不是黑杰克
            else:
                # 庄家是黑杰克，玩家输
                if is_bk(zhuang_list):
                    text += "【[{}](https://t.me/gameyyds)】{}：{} *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                                      generate_choose_status(puke_list),
                                                                                      utils.balance_format(
                                                                                          total_amount))
                    win_count += 1
                    win_amount += total_amount
                    # 庄家不是黑杰克，比大小
                else:
                    # 玩家赢
                    if check_point(puke_list) > check_point(zhuang_list):
                        bonus = total_amount * 1.99
                        text += "【[{}](https://t.me/gameyyds)】{}：{} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                        generate_choose_status(
                                                                                            puke_list),
                                                                                        utils.balance_format(bonus))
                        utils.add_user_balance(user, bonus)
                        utils.increase_win_amount(user, bonus)
                        lose_count += 1
                        lose_amount += (bonus - total_amount)
                        if bonus >= 100000000:
                            reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：赢 +${}\n".format(
                                utils.get_title(user), name, utils.balance_format(bonus))
                    elif check_point(puke_list) == check_point(zhuang_list):
                        bonus = total_amount
                        text += "【[{}](https://t.me/gameyyds)】{}：{} *平局* 退回${}\n".format(utils.get_title(user), name,
                                                                                         generate_choose_status(
                                                                                             puke_list),
                                                                                         utils.balance_format(bonus))
                        utils.add_user_balance(user, bonus)
                    else:
                        text += "【[{}](https://t.me/gameyyds)】{}：{} *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                                          generate_choose_status(
                                                                                              puke_list),
                                                                                          utils.balance_format(
                                                                                              total_amount))
                        win_count += 1
                        win_amount += total_amount
    r.rpush("{}_history".format(group_id), str([win_count, lose_count, win_amount, lose_amount]))
    show_text = "*21点* 本场结算\n\n" \
                "庄家：{}\n\n" \
                "玩家列表：\n" \
                "{}\n" \
                "{}\n".format(generate_choose_status(zhuang_list), text, generate_recent_history(group_id))
    if len(reply_text) > 0:
        return show_text, reply_text, True
    else:
        return show_text, "", False


def is_bk(puke_list):
    if len(puke_list) >= 3:
        return False
    return a_in_puke_list(puke_list) and ten_in_puke_list(puke_list)


def generate_add_money_user(group_id, game_id):
    text = ""
    r = connector.get_connection(15)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, amount = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "total_amount"])
        text += "【[{}](https://t.me/gameyyds)】{}：${}".format(utils.get_title(user), name,
                                                             utils.balance_format(amount)) + "\n"
    return text[:-1]


def generate_choose_status(puke_list):
    text = ""
    for puke in puke_list:
        text = text + "🃏" + num_to_text(puke)
    text = text + " " + check_status(puke_list)
    return text


def generate_choosing_user(group_id, game_id):
    text = ""
    r = connector.get_connection(15)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, choose_num = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "choose_num"])
        choosing_status = r.hget("{}_{}_{}".format(group_id, game_id, user), "choosing_status")
        choose_num = utils.string_to_list(choose_num)
        text = text + "【[{}](https://t.me/gameyyds)】{}：".format(utils.get_title(user), name)
        for puke in choose_num:
            text = text + "🃏" + num_to_text(puke)
        if choosing_status == "0":
            status = " 🔔"
        else:
            status = ""
        text = text + " " + check_status(choose_num) + status + "\n"
    return text


def generate_recent_history(group_id):
    try:
        text = ""
        r = connector.get_connection(15)
        len_history = int(r.llen("{}_history".format(group_id)))
        max_length = 0
        if len_history > 10000:
            len_history = 10000
            max_length = -len_history
        text += "最近{}场历史：\n".format(len_history)
        history = r.lrange("{}_history".format(group_id), max_length, -1)
        if len(history) > 0:
            win_count = 0
            lose_count = 0
            win_amount = 0
            lose_amount = 0
            for rst in history:
                data = utils.string_to_list(rst)
                win_count += data[0]
                lose_count += data[1]
                win_amount += data[2]
                lose_amount += data[3]
            # print(win_count, lose_count, win_amount, lose_amount)
            win_rate = (float(win_count) / float((win_count + lose_count))) * 100
            win_rate = str(win_rate)[:str(win_rate).index(".")] + "%"
            if win_amount >= lose_amount:
                text2 = "*赢* \+$" + utils.balance_format(win_amount - lose_amount)
            else:
                text2 = "*输* ~\-$" + utils.balance_format(lose_amount - win_amount) + "~"
        else:
            win_rate = "暂无数据"
            text2 = "暂无数据"
        text = text + "庄家胜率：" + win_rate + "\n"
        text = text + "庄家输赢：" + text2 + "\n"
        return text[:-1]
    except Exception as e:
        print(e)
        text = "最近历史暂时无法查看\n"
        return text[:-1]


def check_point(puke_list):
    alist = []
    total_point = 0
    for puke in puke_list:
        if num_to_text(puke) == "A":
            alist.append(puke)
        else:
            total_point += num_to_point(puke)
    for apuke in alist:
        if total_point > 21:
            total_point += 1
        elif 11 + total_point > 21:
            total_point += 1
        else:
            total_point += 11
    return total_point


def check_status(puke_list):
    alist = []
    total_point = 0
    for puke in puke_list:
        if num_to_text(puke) == "A":
            alist.append(puke)
        else:
            total_point += num_to_point(puke)
    for apuke in alist:
        if total_point > 21:
            total_point += 1
        elif 11 + total_point > 21:
            total_point += 1
        else:
            total_point += 11
    if total_point > 21:
        return "\(💣爆炸\) 总点数：{}".format(total_point)
    elif is_bk(puke_list):
        return "\(🤡黑杰克\) 总点数：{}".format(total_point)
    else:
        return "总点数：{}".format(total_point)


def is_boom(puke_list):
    alist = []
    total_point = 0
    for puke in puke_list:
        if num_to_text(puke) == "A":
            alist.append(puke)
        else:
            total_point += num_to_point(puke)
    for apuke in alist:
        if total_point > 21:
            total_point += 1
        elif 11 + total_point > 21:
            total_point += 1
        else:
            total_point += 11
    if total_point > 21:
        return True
    else:
        return False


bk_handler = CommandHandler('21', bk)
