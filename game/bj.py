import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random

bot = telegram.Bot(token=TOKEN)


def make_keyboard1(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("1%",
                                 callback_data=str(
                                     'bj_{}_addMoney_0.01_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5%",
                                 callback_data=str(
                                     'bj_{}_addMoney_0.05_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20%",
                                 callback_data=str(
                                     'bj_{}_addMoney_0.2_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50%",
                                 callback_data=str(
                                     'bj_{}_addMoney_0.5_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("80%",
                                 callback_data=str(
                                     'bj_{}_addMoney_0.8_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("梭哈",
                                 callback_data=str(
                                     'bj_{}_addMoney_1.0_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2W", callback_data=str(
                'bj_{}_addMoney_20000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5W", callback_data=str(
                'bj_{}_addMoney_50000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20W", callback_data=str(
                'bj_{}_addMoney_200000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50W", callback_data=str(
                'bj_{}_addMoney_500000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200W", callback_data=str(
                'bj_{}_addMoney_2000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500W", callback_data=str(
                'bj_{}_addMoney_5000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("2KW", callback_data=str(
                'bj_{}_addMoney_20000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KW", callback_data=str(
                'bj_{}_addMoney_50000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2E", callback_data=str(
                'bj_{}_addMoney_200000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5E", callback_data=str(
                'bj_{}_addMoney_500000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("20E", callback_data=str(
                'bj_{}_addMoney_2000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50E", callback_data=str(
                'bj_{}_addMoney_5000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("200E", callback_data=str(
                'bj_{}_addMoney_20000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500E", callback_data=str(
                'bj_{}_addMoney_50000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KE", callback_data=str(
                'bj_{}_addMoney_200000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5KE", callback_data=str(
                'bj_{}_addMoney_500000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2WE", callback_data=str(
                'bj_{}_addMoney_2000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5WE", callback_data=str(
                'bj_{}_addMoney_5000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("20WE", callback_data=str(
                'bj_{}_addMoney_20000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50WE", callback_data=str(
                'bj_{}_addMoney_50000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("200WE", callback_data=str(
                'bj_{}_addMoney_200000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("500WE", callback_data=str(
                'bj_{}_addMoney_500000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2KWE", callback_data=str(
                'bj_{}_addMoney_2000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5kWE", callback_data=str(
                'bj_{}_addMoney_5000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("1EE", callback_data=str(
                'bj_{}_addMoney_10000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("3EE", callback_data=str(
                'bj_{}_addMoney_30000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5EE", callback_data=str(
                'bj_{}_addMoney_50000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("10EE", callback_data=str(
                'bj_{}_addMoney_100000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("50EE", callback_data=str(
                'bj_{}_addMoney_500000000000000000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("99EE", callback_data=str(
                'bj_{}_addMoney_990000000000000000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("▶️开始游戏",
                                 callback_data=str(
                                     'bj_{}_startGame_{}_None_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
            InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
        ]
    ]


def make_keyboard2(group_id, timestamp):
    return [[
        InlineKeyboardButton("庄🔴",
                             callback_data=str(
                                 'bj_{}_choosingNum_1_{}_None_None'.format(group_id, timestamp))),
        InlineKeyboardButton("和🟢",
                             callback_data=str(
                                 'bj_{}_choosingNum_3_{}_None_None'.format(group_id, timestamp))),
        InlineKeyboardButton("闲🔵",
                             callback_data=str(
                                 'bj_{}_choosingNum_2_{}_None_None'.format(group_id, timestamp))),
    ], [
        InlineKeyboardButton("庄对🟠",
                             callback_data=str(
                                 'bj_{}_choosingNum_4_{}_None_None'.format(group_id, timestamp))),
        InlineKeyboardButton("闲对🟣",
                             callback_data=str(
                                 'bj_{}_choosingNum_5_{}_None_None'.format(group_id, timestamp))),
    ], [
        InlineKeyboardButton("结算游戏",
                             callback_data=str(
                                 'bj_{}_endGame_{}_None_None_None'.format(group_id, timestamp))),
    ]]


def num_to_text(num):
    text = ["", "庄", "闲", "和", "庄对", "闲对"]
    return text[int(num)]


def num_to_symbol(num):
    symbol = ["", "🔴", "🔵", "🟢", "🟠", "🟣"]
    return symbol[int(num)]


def num_to_int(num):
    text = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "0", "0", "0"]
    return int(text[int(num)])


def num_to_puke(num):
    puke = ["", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
    return puke[int(num)]


def four_random_nums():
    rst = []
    for i in range(4):
        rst.append(random.randint(1, 13))
    return utils.list_to_string(rst)


def process(update, context):
    query = update.callback_query
    try:
        game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
        if action == "startGame":
            start_game(update)
        elif action == "addMoney":
            add_money(update, context)
        elif action == "choosingNum":
            choosing(update, context)
        elif action == "endGame":
            end_game(update, context)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")


def bj(update, context):
    try:
        message = utils.ana_message(update)
        if utils.is_user_in_black_list(message.user_id):
            update.message.reply_text("黑名单用户，无权操作")
            return
        message = utils.ana_message(update)
        chat_type = message.group_type
        user_id = message.user_id
        chat_id = message.group_id
        # 限制为群组
        if chat_type != "supergroup" and chat_type != "group":
            update.message.reply_text("此命令只有在群组中有效")
            return
        utils.get_user_balance_update(user_id, update)
        timestamp = utils.get_timestamp()
        r = connector.get_connection(9)
        if utils.new_game_lock_existed(chat_id):
            bot.sendMessage(
                chat_id=chat_id,
                text="开局受限\n每个会话窗口每隔90s才可新建一局\n上一局游戏完成结算后立即解锁新局"
            )
            return
        r.hmset("{}_{}".format(chat_id, timestamp), {
            "status": "created",
            "num": four_random_nums(),
            "participant": utils.list_to_string([])
        })
        utils.set_new_game_lock(chat_id)
        reply_markup = InlineKeyboardMarkup(make_keyboard1(chat_id, timestamp))
        bot.sendMessage(
            chat_id=utils.ana_message(update).group_id,
            text="*百家乐* 正在投注\n\n{}".format(utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
    except Exception as e:
        print(e)


def start_game(update):
    print("start_game")
    r = connector.get_connection(9)
    message = utils.ana_message(update)
    game_name, group_id, action, game_id, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
    status, num, participant = r.hmget("{}_{}".format(message.group_id, game_id), ['status', 'num', 'participant'])
    print("获取到 Redis 中的数据", status, num, participant)
    participant = utils.string_to_list(participant)
    if status == "choosing":
        rest_time = r.ttl("{}_{}_time_limit".format(message.group_id, game_id))
        if "-" in str(rest_time):
            rest_time = 0
        print("由于更新问题，重新向未更新用户推送键盘")
        query = update.callback_query
        reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
        query.edit_message_text(
            text="*百家乐* 选择中\({}\)\n\n"
                 "玩家列表：\n"
                 "{}\n"
                 "{}\n\n{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, game_id)),
                                   generate_choosing_user(message.group_id, game_id),
                                   generate_recent_history(message.group_id), utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
        return
    if status == "end":
        utils.show_msg(update, "游戏已经结束", True)
        return
    if len(participant) == 0:
        utils.show_msg(update, "目前没有用户下注，请等待用户下注后开始游戏", True)
        return
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return
    if r.exists("{}_{}_start_lock".format(message.group_id, game_id)):
        utils.show_msg(update, "所有用户无操作6s后才能开始游戏", True)
        return
    r.hset("{}_{}".format(message.group_id, game_id), "status", "choosing")
    # 设置游戏选择时间
    r.set("{}_{}_time_limit".format(message.group_id, game_id), "", ex=60)
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
    query.edit_message_text(
        text="*百家乐* 选择中\({}\)\n\n"
             "玩家列表：\n"
             "{}\n"
             "{}\n\n{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, game_id)),
                               generate_choosing_user(message.group_id, game_id),
                               generate_recent_history(message.group_id), utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )


def add_money(update, context):
    message = utils.ana_message(update)
    balance = utils.get_user_balance_update(message.user_id, update)
    if balance <= 0:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(9)
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != "created":
        utils.show_msg(update, "下注已结束，无法下注！", True)
        return
    total_amount, add_money_times, choose_times = 0, 0, 0
    print("游戏ID" + str(query_data.arg2))
    print("下注数据是否存在：" + str(r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id))))
    if r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id)):
        print("数据库读取下注数据")
        print(r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"]))
        total_amount, add_money_times, choose_times = r.hmget(
            "{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id),
            ["total_amount", "add_money_times", "choose_times"])
    else:
        print("第一次下注，添加数据")
        r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
            "name": message.name,
            "total_amount": 0,
            "add_money_times": 0,
            "choose_times": 0,
            "choose_num": -1,
        })
    total_amount = int(total_amount)
    add_money_times = int(add_money_times)
    if add_money_times >= 5:
        utils.show_msg(update, "加注次数超过5次，此次加注失败！", True)
        return
    query_data = utils.ana_query_data(update)
    add_amount = query_data.arg1
    print("用户选择的下注金额：" + add_amount)
    if add_amount == "1.0":
        add_amount = balance
    elif "." in add_amount:
        add_amount = balance * float(add_amount)
        print("百分比折算后的下注金额：" + str(add_amount))
    print("之前下注总金额：" + str(total_amount))
    print("此次下注金额：" + str(add_amount))
    add_amount = int(add_amount)
    if balance < add_amount:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    utils.reduce_user_balance(message.user_id, add_amount)
    total_amount += add_amount
    print("当前总下注金额：" + str(total_amount))
    add_money_times += 1
    r.set("{}_{}_start_lock".format(message.group_id, query_data.arg2), "", ex=6)
    r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
        "total_amount": total_amount,
        "add_money_times": add_money_times,
        "choose_times": 0
    })
    participant = utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), 'participant'))
    if message.user_id not in participant:
        participant.append(message.user_id)
    r.hset("{}_{}".format(message.group_id, query_data.arg2), "participant", utils.list_to_string(participant))
    utils.show_msg(update, "下注成功\n您当前下注总额：${}\n请在无人跟注6s后点击开始游戏!".format(utils.balance_format(total_amount)), True)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    #   更新显示文字
    reply_markup = InlineKeyboardMarkup(make_keyboard1(message.group_id, query_data.arg2))
    query = update.callback_query
    query.edit_message_text(
        text="*百家乐* 正在投注\n\n玩家列表：\n" + generate_add_money_user(message.group_id, query_data.arg2) + "\n\n{}".format(
            utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)


def choosing(update, context):
    print("choosing")
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(9)
    print("选择时间限制，键：" + str(r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2))))
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != "choosing":
        utils.show_msg(update, "此局游戏已经结算！", True)
        return
    if not r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        utils.show_msg(update, "选择阶段已经结束，请尽快结算本局游戏", True)
        return
    if int(r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times")) >= 4:
        utils.show_msg(update, "选择最多只能更改3次，此次更改失败", True)
        return
    choose_num = query_data.arg1
    print("用户选择的为:" + choose_num)
    # 更新用户选择的数字
    print("更新用户选择的数字")
    r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_num", choose_num)
    choose_times = int(r.hget("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times"))
    choose_times += 1
    print("更新用户选择次数")
    r.hset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), "choose_times", choose_times)
    # 设置冷却时间
    print("设置结算游戏锁定")
    r.set("{}_{}_end_lock".format(message.group_id, query_data.arg2), "", ex=6)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, query_data.arg2))
    query.edit_message_text(
        text="*百家乐* 选择中\({}\)\n\n"
             "玩家列表：\n"
             "{}\n"
             "{}\n\n"
             "{}".format(r.ttl("{}_{}_time_limit".format(message.group_id, query_data.arg2)),
                         generate_choosing_user(message.group_id, query_data.arg2),
                         generate_recent_history(message.group_id), utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)


def end_game(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(9)
    if not utils.end_game_check(update, query_data.arg1, 9):
        return
    print("开始进入结算流程")
    # 将游戏设置为结束状态
    r.hset("{}_{}".format(message.group_id, query_data.arg1), "status", "end")
    rst_message_id = update.callback_query.message.message_id
    print("开始计算用户余额")
    rst_message_id = update.callback_query.message.message_id
    show_text, reply_text, is_reply = calculate(message.group_id, query_data.arg1)
    keyboard = [[
        InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
        InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    print(show_text)
    print("准备返回数据")
    query = update.callback_query
    query.edit_message_text(
        text=show_text + "\n\n{}".format(utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    utils.del_new_game_lock(message.group_id)
    if is_reply:
        try:
            bot.sendMessage(
                chat_id=message.group_id,
                text="🎉🎉🎉祝贺本场*百家乐* 大赢家\n\n" + reply_text,
                reply_to_message_id=rst_message_id,
                parse_mode="Markdown"
            )
        except Exception as e:
            print(e)


def calculate(group_id, timestamp):
    print("calculate")
    r = connector.get_connection(9)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, timestamp), "participant"))
    num = utils.string_to_list(r.hget("{}_{}".format(group_id, timestamp), "num"))
    zhuang_num1 = num[0]
    zhuang_num2 = num[1]
    xian_num1 = num[2]
    xian_num2 = num[3]
    print(zhuang_num1, zhuang_num2, xian_num1, xian_num2)
    zhuang_point = (num_to_int(zhuang_num1) + num_to_int(zhuang_num2)) % 10
    xian_point = (num_to_int(xian_num1) + num_to_int(xian_num2)) % 10
    print(zhuang_point, xian_point)
    print("系统生成的庄闲为：" + str(num))
    print(zhuang_point, xian_point)
    text = ""
    rst = ""
    reply_text = ""
    history = 1
    if zhuang_point > xian_point:
        history = 1
    elif zhuang_point < xian_point:
        history = 2
    elif zhuang_point == xian_point:
        history = 3
    r.rpush("{}_history".format(group_id), history)
    print("赢家：{}".format(history))
    flag = False
    for user in participant:
        if (r.hget("{}_{}_{}".format(group_id, timestamp, user), "choose_num")) == '3':
            flag = True
            break
    print("是否有人选和：{}".format(flag))
    for user in participant:
        choose_num, total_amount, name = r.hmget("{}_{}_{}".format(group_id, timestamp, user),
                                                 ["choose_num", "total_amount", "name"])
        choose_num = int(choose_num)
        total_amount = int(total_amount)
        bonus = 0
        if choose_num == -1:
            print("该用户未做出选择，返还余额")
            utils.add_user_balance(user, total_amount)
            text += "【[{}](https://t.me/gameyyds)】{}：*退回* ${}\n".format(utils.get_title(user), name,
                                                                        utils.balance_format(total_amount))
        else:
            bonus = 0
            # 如果选择 庄对 闲对
            if choose_num in [4, 5]:
                # 判断正确
                if (choose_num == 4 and zhuang_num1 == zhuang_num2) or (choose_num == 5 and xian_num1 == xian_num2):
                    bonus = total_amount * 12
                    utils.add_user_balance(user, bonus)
                    utils.increase_win_amount(user, bonus)
                    text += "【[{}](https://t.me/gameyyds)】{}：{} {} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                       num_to_text(choose_num),
                                                                                       num_to_symbol(choose_num),
                                                                                       utils.balance_format(bonus))
                    if bonus >= 100000000:
                        reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：*赢* +${}\n".format(
                            utils.get_title(user), name, utils.balance_format(bonus))
                else:
                    text += "【[{}](https://t.me/gameyyds)】{}：{} {} *输* ~\-${}~\n".format(utils.get_title(user), name,
                                                                                         num_to_text(choose_num),
                                                                                         num_to_symbol(choose_num),
                                                                                         utils.balance_format(
                                                                                             total_amount))
            # 选择其他三项
            else:
                # 如果开奖结果为 和
                if history == 3:
                    # 如果有人选 和
                    if flag:
                        # 如果他选的是 庄 和 闲
                        if choose_num in [1, 2]:
                            # 错误
                            text += "【[{}](https://t.me/gameyyds)】{}：{} {} *输* ~\-${}~\n".format(utils.get_title(user),
                                                                                                 name,
                                                                                                 num_to_text(
                                                                                                     choose_num),
                                                                                                 num_to_symbol(
                                                                                                     choose_num),
                                                                                                 utils.balance_format(
                                                                                                     total_amount))
                        elif choose_num == 3:
                            bonus = total_amount * 9
                            utils.add_user_balance(user, bonus)
                            utils.increase_win_amount(user, bonus)
                            text += "【[{}](https://t.me/gameyyds)】{}：{} {} *赢* \+${}\n".format(utils.get_title(user),
                                                                                               name,
                                                                                               num_to_text(choose_num),
                                                                                               num_to_symbol(
                                                                                                   choose_num),
                                                                                               utils.balance_format(
                                                                                                   bonus))
                            if bonus >= 100000000:
                                reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：*赢* +${}\n".format(
                                    utils.get_title(user), name, utils.balance_format(bonus))
                    # 无人选和
                    else:
                        utils.add_user_balance(user, total_amount)
                        text += "【[{}](https://t.me/gameyyds)】{}：{} {} *退回* ${}\n".format(utils.get_title(user), name,
                                                                                          num_to_text(choose_num),
                                                                                          num_to_symbol(choose_num),
                                                                                          utils.balance_format(
                                                                                              total_amount))
                elif history in [1, 2]:
                    # 选 庄 正确
                    if choose_num == history and choose_num == 1:
                        bonus = int(total_amount * 1.95)
                        utils.add_user_balance(user, bonus)
                        utils.increase_win_amount(user, bonus)
                        text += "【[{}](https://t.me/gameyyds)】{}：{} {} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                           num_to_text(choose_num),
                                                                                           num_to_symbol(choose_num),
                                                                                           utils.balance_format(bonus))
                        if bonus >= 100000000:
                            reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：*赢* +${}\n".format(
                                utils.get_title(user), name, utils.balance_format(bonus))
                    elif choose_num == history and choose_num == 2:
                        bonus = total_amount * 2
                        utils.add_user_balance(user, bonus)
                        utils.increase_win_amount(user, bonus)
                        text += "【[{}](https://t.me/gameyyds)】{}：{} {} *赢* \+${}\n".format(utils.get_title(user), name,
                                                                                           num_to_text(choose_num),
                                                                                           num_to_symbol(choose_num),
                                                                                           utils.balance_format(bonus))
                        if bonus >= 100000000:
                            reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：*赢* +${}\n".format(
                                utils.get_title(user), name, utils.balance_format(bonus))
                    else:
                        text += "【[{}](https://t.me/gameyyds)】{}：{} {} *输* ~\-${}~\n".format(utils.get_title(user),
                                                                                             name,
                                                                                             num_to_text(choose_num),
                                                                                             num_to_symbol(choose_num),
                                                                                             utils.balance_format(
                                                                                                 total_amount))
    show_text = "*百家乐* 本场结算\n\n" \
                "🔴庄\({}\+{}\)\n" \
                "🔵闲\({}\+{}\)\n" \
                "牌面：\n{}{}赢\n\n" \
                "玩家列表：\n" \
                "{}\n" \
                "{}".format(num_to_puke(zhuang_num1), num_to_puke(zhuang_num2),
                            num_to_puke(xian_num1), num_to_puke(xian_num2),
                            num_to_symbol(history), num_to_text(history),
                            text,
                            generate_recent_history(group_id))
    if len(reply_text) > 0:
        return show_text, reply_text, True
    else:
        return show_text, "", False


def generate_add_money_user(group_id, game_id):
    text = ""
    r = connector.get_connection(9)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, amount = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "total_amount"])
        text += "【[{}](https://t.me/gameyyds)】{}：${}".format(utils.get_title(user), name,
                                                             utils.balance_format(amount)) + "\n"
    return text[:-1]


def generate_choosing_user(group_id, game_id):
    text = ""
    r = connector.get_connection(9)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, choose_num = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "choose_num"])
        if choose_num == '-1':
            text += "【[{}](https://t.me/gameyyds)】{}：🕒 未选择\n".format(utils.get_title(user), name)
        else:
            text += "【[{}](https://t.me/gameyyds)】{}：✅ {}{}\n".format(utils.get_title(user), name,
                                                                      num_to_text(choose_num),
                                                                      num_to_symbol(choose_num))
    return text


def generate_recent_history(group_id):
    text = ""
    r = connector.get_connection(9)
    len_history = int(r.llen("{}_history".format(group_id)))
    max_length = 0
    if len_history > 15:
        len_history = 15
        max_length = -len_history
    text += "最近{}场历史：\n".format(len_history)
    history = r.lrange("{}_history".format(group_id), max_length, -1)
    for rst in history:
        text += num_to_symbol(rst) + " "
    print(text)
    return text[:-1]


bj_handler = CommandHandler('bj', bj)
