import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random
from enum import Enum

bot = telegram.Bot(token=TOKEN)

# 起始金额
START_MONEY = 10000

# 跟注时间限制
ADD_MONEY_TIME_LIMIT = 10


class GameStatus(Enum):
    WAITING_START = "1"
    ADDING_MONEY = "2"
    END = "3"


class LookPuke(Enum):
    NOT_LOOK = "0"
    LOOKED = "1"


class UserStatus(Enum):
    WAITING_ADD_MONEY = "0"
    ADDED_MONEY = "1"
    EXITED = "2"


def get_redis_dict_data(group_id, game_id, user_id, key):
    r = connector.get_connection(19)
    if str(user_id) == "0":
        rst = r.hget("{}_{}".format(group_id, game_id), key)
        return rst
    else:
        rst = r.hget("{}_{}_{}".format(group_id, game_id, user_id), key)
        return rst


def update_redis_dict_data(group_id, game_id, user_id, key, value):
    r = connector.get_connection(19)
    if str(user_id) == "0":
        r.hset("{}_{}".format(group_id, game_id), key, str(value))
    else:
        r.hset("{}_{}_{}".format(group_id, game_id, user_id), key, str(value))


def make_keyboard1(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("点击垫底 ${}".format(utils.balance_format(START_MONEY)), callback_data=str(
                'zjh2_{}_joinGame_{}_{}_None_None'.format(group_id, START_MONEY, timestamp))),
        ],
        [
            InlineKeyboardButton("▶️开始游戏",
                                 callback_data=str(
                                     'zjh2_{}_startGame_{}_None_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
            InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
        ]
    ]


def make_keyboard2(group_id, timestamp):
    return [
        [
            InlineKeyboardButton("1W", callback_data=str(
                'zjh2_{}_addMoney_10000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("2W", callback_data=str(
                'zjh2_{}_addMoney_20000_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("5W", callback_data=str(
                'zjh2_{}_addMoney_50000_{}_None_None'.format(group_id, timestamp))),
        ],
        [
            InlineKeyboardButton("开牌",
                                 callback_data=str(
                                     'zjh2_{}_endGame_A_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("看牌",
                                 callback_data=str(
                                     'zjh2_{}_seePuke_A_{}_None_None'.format(group_id, timestamp))),
            InlineKeyboardButton("出局",
                                 callback_data=str(
                                     'zjh2_{}_exitGame_A_{}_None_None'.format(group_id, timestamp)))
        ],
        [
            InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
            InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
        ]
    ]


def get_random_num():
    return random.randint(1, 52)


def num_to_text(num):
    num = int(num) % 13
    alist = ["K", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q"]
    return alist[num]


def num_to_point(num):
    num = int(int(num) % 13)
    alist = [13, 14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    return alist[num]


def num_to_color_level(num):
    num = int((int(num) - 1) / 13)
    if num == 0 or num == 2:
        return 1
    else:
        return 2


def num_to_color_symbol(num):
    alist = ["♥️", "♠️", "♦️", "♣️"]
    return alist[int(int(num) / 13)]


def status_to_text(status):
    if str(status) == UserStatus.WAITING_ADD_MONEY.value:
        return "待跟注"
    elif str(status) == UserStatus.ADDED_MONEY.value:
        return "已跟注"
    elif str(status) == UserStatus.EXITED.value:
        return "已出局"


def look_to_text(look):
    if str(look) == LookPuke.NOT_LOOK.value:
        return "未看牌"
    elif str(look) == LookPuke.LOOKED.value:
        return "已看牌"


def show_user_status(status):
    if str(status) == UserStatus.WAITING_ADD_MONEY.value:
        return "选择中⏰"
    elif str(status) == UserStatus.ADDED_MONEY.value:
        return "已跟注✅"
    elif str(status) == UserStatus.EXITED.value:
        return "已出局❌"


def generate_user_puke(group_id, game_id):
    used_puke = utils.string_to_list(get_redis_dict_data(group_id, game_id, 0, "used_puke"))
    print("使用过的扑克" + str(used_puke))
    pukes = []
    for i in range(3):
        while True:
            puke = get_random_num()
            if puke not in used_puke:
                used_puke.append(puke)
                pukes.append(puke)
                break
    sort_puke = []
    for i in pukes:
        sort_puke.append([i, num_to_point(i)])
    sort_puke = sorted(sort_puke, key=lambda kv: (int(kv[1])))
    pukes.clear()
    for i in sort_puke:
        pukes.append(i[0])
    print(pukes[0], pukes[1], pukes[2])
    print("更新使用过的扑克" + str(used_puke))
    update_redis_dict_data(group_id, game_id, 0, "used_puke", str(used_puke))
    return pukes[0], pukes[1], pukes[2]


def process(update, context):
    query = update.callback_query
    try:
        game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
        if action == "startGame":
            start_game(update)
        elif action == "joinGame":
            join_game(update, context)
        elif action == "addMoney":
            add_money(update, context)
        elif action == "endGame":
            end_game_prepare(update, context)
        elif action == "exitGame":
            exit_game(update, context)
        elif action == "seePuke":
            see_puke(update, context)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")


def zjh2(update, context):
    try:
        message = utils.ana_message(update)
        if utils.is_user_in_black_list(message.user_id):
            update.message.reply_text("黑名单用户，无权操作")
            return
        chat_type = message.group_type
        user_id = message.user_id
        chat_id = message.group_id
        # 限制为群组
        if chat_type != "supergroup" and chat_type != "group":
            update.message.reply_text("此命令只有在群组中有效")
            return
        utils.get_user_balance_update(user_id, update)
        timestamp = utils.get_timestamp()
        r = connector.get_connection(19)
        if utils.new_game_lock_existed(chat_id):
            bot.sendMessage(
                chat_id=chat_id,
                text="开局受限\n每个会话窗口每隔90s才可新建一局\n上一局游戏完成结算后立即解锁新局"
            )
            return
        r.hmset("{}_{}".format(chat_id, timestamp), {
            "status": GameStatus.WAITING_START.value,
            "circle": 1,
            "last_add_money": START_MONEY,
            "used_puke": str([]),
            "now_user": 0,
            "participant": utils.list_to_string([])
        })
        utils.set_new_game_lock(chat_id)
        reply_markup = InlineKeyboardMarkup(make_keyboard1(chat_id, timestamp))
        bot.sendMessage(
            chat_id=utils.ana_message(update).group_id,
            text="*炸金花* 正在投注\n\n{}".format(utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
    except Exception as e:
        print(e)


def start_game(update):
    print("start_game")
    r = connector.get_connection(19)
    message = utils.ana_message(update)
    game_name, group_id, action, game_id, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
    status, participant = r.hmget("{}_{}".format(message.group_id, game_id), ['status', 'participant'])
    print("开始游戏｜获取到 Redis 中的数据", status, participant)
    participant = utils.string_to_list(participant)
    if len(participant) < 2:
        utils.show_msg(update, "人数不足两人，无法开始游戏", True)
        return
    if status == GameStatus.ADDING_MONEY.value:
        # rest_time = r.ttl("{}_{}_time_limit".format(message.group_id, game_id))
        # if "-" in str(rest_time):
        #     rest_time = 0
        print("由于更新问题，重新向未更新用户推送键盘")
        query = update.callback_query
        reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
        query.edit_message_text(
            text="*炸金花* 游戏中\n\n"
                 "上家跟注金额：\${}\n\n"
                 "玩家列表：\n"
                 "{}\n\n"
                 "{}".format(utils.balance_format(get_redis_dict_data(group_id, game_id, 0, "last_add_money")),
                             generate_choosing_user(message.group_id, game_id),
                             utils.get_random_ad()),
            reply_markup=reply_markup,
            parse_mode="MarkdownV2",
            disable_web_page_preview=True
        )
        return
    if status == GameStatus.END.value:
        utils.show_msg(update, "游戏已经结束", True)
        return
    if len(participant) == 0:
        utils.show_msg(update, "目前没有用户下注，请等待用户下注后开始游戏", True)
        return
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return
    if r.exists("{}_{}_start_lock".format(message.group_id, game_id)):
        utils.show_msg(update, "所有用户无操作6s后才能开始游戏", True)
        return
    update_redis_dict_data(group_id, game_id, 0, "status", GameStatus.ADDING_MONEY.value)
    # 设置游戏选择时间
    # r.set("{}_{}_time_limit".format(message.group_id, game_id), "", ex=60)
    query = update.callback_query
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, game_id))
    for user in participant:
        order = get_redis_dict_data(group_id, game_id, user, "order")
        if int(order) == 0:
            update_redis_dict_data(group_id, game_id, 0, "now_user", user)
            r.set("{}_{}_time_limit".format(group_id, game_id), "", ex=ADD_MONEY_TIME_LIMIT)
            update_redis_dict_data(group_id, game_id, user, "status", UserStatus.WAITING_ADD_MONEY.value)
    query.edit_message_text(
        text="*炸金花* 游戏中\n\n"
             "上家跟注金额：${}\n\n"
             "玩家列表：\n"
             "{}\n\n"
             "{}".format(utils.balance_format(get_redis_dict_data(group_id, game_id, 0, "last_add_money")),
                         generate_choosing_user(message.group_id, game_id),
                         utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )


def join_game(update, context):
    message = utils.ana_message(update)
    balance = utils.get_user_balance_update(message.user_id, update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(19)
    if balance <= 0 or balance < START_MONEY:
        utils.show_msg(update, "余额不足，请通过签到获取资金后下注", True)
        return
    participant = utils.string_to_list(r.hget("{}_{}".format(message.group_id, query_data.arg2), 'participant'))
    if len(participant) > 15:
        utils.show_msg(update, "此轮游戏人数已达上限，无法下注", True)
        return
    if message.user_id in participant:
        utils.show_msg(update, "您已经投注，不可再次投注", True)
        return
    status = r.hget("{}_{}".format(message.group_id, query_data.arg2), "status")
    if status != GameStatus.WAITING_START.value:
        utils.show_msg(update, "下注已结束，无法下注！", True)
        return
    print("游戏ID" + str(query_data.arg2))
    print("下注数据是否存在：" + str(r.exists("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id))))
    puke1, puke2, puke3 = generate_user_puke(message.group_id, query_data.arg2)
    level = get_puke_level(puke1, puke2, puke3)
    r.hmset("{}_{}_{}".format(message.group_id, query_data.arg2, message.user_id), {
        "puke1": puke1,
        "puke2": puke2,
        "puke3": puke3,
        "total_amount": START_MONEY,
        "name": message.name,
        "look": LookPuke.NOT_LOOK.value,
        "status": UserStatus.ADDED_MONEY.value,
        "level": level,
        "order": len(participant)
    })
    print("此次下注金额：" + str(START_MONEY))
    utils.reduce_user_balance(message.user_id, START_MONEY)
    total_amount = START_MONEY
    participant.append(message.user_id)
    update_redis_dict_data(message.group_id, query_data.arg2, 0, "participant", participant)
    utils.show_msg(update, "下注成功\n您当前下注总额：${}\n请在无人跟注6s后点击开始游戏!".format(utils.balance_format(total_amount)), True)
    print("设置刷新锁定")
    print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
    if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        print("存在锁定，跳过刷新")
        return
    #   更新显示文字
    reply_markup = InlineKeyboardMarkup(make_keyboard1(message.group_id, query_data.arg2))
    query = update.callback_query
    query.edit_message_text(
        text="*炸金花* 正在投注\n\n玩家列表：\n" + generate_add_money_user(message.group_id, query_data.arg2) + "\n\n{}".format(
            utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)
    r.set("{}_{}_start_lock".format(message.group_id, query_data.arg2), "", ex=6)


def update_keyboard(update, context):
    query = update.callback_query
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    reply_markup = InlineKeyboardMarkup(make_keyboard2(message.group_id, query_data.arg2))
    query.edit_message_text(
        text="*炸金花* 游戏中\n\n"
             "上家跟注金额：\${}\n\n"
             "玩家列表：\n"
             "{}\n\n"
             "{}".format(
            utils.balance_format(get_redis_dict_data(message.group_id, query_data.arg2, 0, "last_add_money")),
            generate_choosing_user(message.group_id, query_data.arg2),
            utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )


def add_money(update, context):
    print("choosing")
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    r = connector.get_connection(19)
    print("选择时间限制，键：" + str(r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2))))
    status = get_redis_dict_data(message.group_id, query_data.arg2, 0, "status")
    if status != GameStatus.ADDING_MONEY.value:
        utils.show_msg(update, "此局游戏已经结算！", True)
        return
    participant = utils.string_to_list(get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant"))
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此轮游戏，无法操作", True)
        return
    user_status = get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "status")
    if user_status == UserStatus.EXITED.value:
        utils.show_msg(update, "您已经出局，无法操作", True)
        return
    now_user = get_redis_dict_data(message.group_id, query_data.arg2, 0, "now_user")
    if now_user != str(message.user_id) and r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        utils.show_msg(update, "当前还未轮到您跟注，请等待！", True)
        if is_game_over(message.group_id, query_data.arg2):
            end_game(update, context)
            return
        return
    if now_user != str(message.user_id) and not r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        # 上轮用户操作超时，自动出局，轮到下一位用户操作
        update_redis_dict_data(message.group_id, query_data.arg2, now_user, "status", UserStatus.EXITED.value)
        utils.show_msg(update, "在您之前的用户由于超时已经自动出局，状态已经刷新，如果下一位是您，请重新选择跟注金额！", True)
        if is_game_over(message.group_id, query_data.arg2):
            end_game(update, context)
            return
        find_next_user_to_add_money(message.group_id, query_data.arg2)
        update_keyboard(update, context)
        r.set("{}_{}_time_limit".format(message.group_id, query_data.arg2), "", ex=ADD_MONEY_TIME_LIMIT)
        return
    if now_user == str(message.user_id) and not r.exists("{}_{}_time_limit".format(message.group_id, query_data.arg2)):
        # 用户操作超时，自动出局，轮到下一位用户操作
        utils.show_msg(update, "抱歉，由于您超时未选择，已经自动出局！", True)
        update_redis_dict_data(message.group_id, query_data.arg2, now_user, "status", UserStatus.EXITED.value)
        if is_game_over(message.group_id, query_data.arg2):
            end_game(update, context)
            return
        find_next_user_to_add_money(message.group_id, query_data.arg2)
        update_keyboard(update, context)
        r.set("{}_{}_time_limit".format(message.group_id, query_data.arg2), "", ex=ADD_MONEY_TIME_LIMIT)
        return
    circle = int(get_redis_dict_data(message.group_id, query_data.arg2, 0, "circle"))
    if circle > 5:
        utils.show_msg(update, "此轮游戏最多加注5轮", True)
        return
    # 轮到此用户跟注
    user_choose_amount = int(query_data.arg1)
    balance = utils.get_user_balance(message.user_id)
    if balance < user_choose_amount:
        utils.show_msg(update, "余额不足，请签到获取资金后跟注！", True)
        return
    if is_game_over(message.group_id, query_data.arg2):
        end_game(update, context)
        return
    last_add_money = int(get_redis_dict_data(message.group_id, query_data.arg2, 0, "last_add_money"))
    if int(user_choose_amount) < last_add_money:
        utils.show_msg(update, "跟注金额不能小于上家跟注金额，请重新选择！", True)
        return
    # 所有条件都符合，更新数据
    # 更新余额
    utils.reduce_user_balance(message.user_id, user_choose_amount)
    total_amount = int(get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "total_amount"))
    after_amount = total_amount + user_choose_amount
    update_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "total_amount", after_amount)
    # 更新状态
    update_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "status", UserStatus.ADDED_MONEY.value)
    # 更新上家跟注金额
    update_redis_dict_data(message.group_id, query_data.arg2, 0, "last_add_money", user_choose_amount)
    check_circle(message.group_id, query_data.arg2, circle, now_user)
    circle = int(get_redis_dict_data(message.group_id, query_data.arg2, 0, "circle"))
    if is_game_over(message.group_id, query_data.arg2):
        end_game(update, context)
        return
    # 判断跟注轮次是否大于5次
    if circle >= 6:
        end_game(update, context)
        return
    else:
        # 更新下一位用户ID
        find_next_user_to_add_money(message.group_id, query_data.arg2)
        print("设置刷新锁定")
        print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
        if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
            print("存在锁定，跳过刷新")
            return
        update_keyboard(update, context)
        r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=1)
        utils.show_msg(update, "您此次跟注金额为：${}\n总金额：${}".format(
            utils.balance_format(user_choose_amount), utils.balance_format(after_amount)
        ), True)
        r.set("{}_{}_time_limit".format(message.group_id, query_data.arg2), "", ex=ADD_MONEY_TIME_LIMIT)


def end_game(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    update_keyboard(update, context)
    participant = get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant")
    status = get_redis_dict_data(message.group_id, query_data.arg2, 0, "status")
    participant = utils.string_to_list(participant)
    if status != GameStatus.ADDING_MONEY.value:
        utils.show_msg(update, "此局游戏已经结算！由于系统限制，显示文字刷新失败，但是结算金额已经到账！", True)
        return
    print("开始判断用户是否参与了此局游戏")
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权更改游戏状态", True)
        return
    print("开始进入结算流程")
    # 将游戏设置为结束状态
    update_redis_dict_data(message.group_id, query_data.arg2, 0, "status", GameStatus.END.value)
    print("开始计算用户余额")
    rst_message_id = update.callback_query.message.message_id
    show_text, reply_text, is_reply = calculate(update, context)
    keyboard = [[
        InlineKeyboardButton("💰️查看余额", callback_data=str('show_balance')),
        InlineKeyboardButton("✅️签到领钱", callback_data=str('sign')),
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    # print(show_text)
    print("准备返回数据")
    query = update.callback_query
    query.edit_message_text(
        text=show_text + "\n{}".format(utils.get_random_ad()),
        reply_markup=reply_markup,
        parse_mode="MarkdownV2",
        disable_web_page_preview=True
    )
    utils.del_new_game_lock(message.group_id)
    if is_reply:
        try:
            bot.sendMessage(
                chat_id=message.group_id,
                text="🎉🎉🎉祝贺本场*炸金花* 大赢家\n\n" + reply_text,
                reply_to_message_id=rst_message_id,
                parse_mode="Markdown"
            )
        except Exception as e:
            print(e)


def exit_game(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    # r = connector.get_connection(19)
    participant = utils.string_to_list(get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant"))
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权操作！", True)
        return
    if get_redis_dict_data(message.group_id, query_data.arg2, 0, "status") != GameStatus.ADDING_MONEY.value:
        utils.show_msg(update, "游戏已经结束", True)
        return
    now_user = get_redis_dict_data(message.group_id, query_data.arg2, 0, "now_user")
    if now_user != str(message.user_id):
        utils.show_msg(update, "当前没有轮到您操作，请等待！", True)
        return
    user_status = get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "status")
    if user_status == UserStatus.WAITING_ADD_MONEY.value:
        update_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "status", UserStatus.EXITED.value)
        if is_game_over(message.group_id, query_data.arg2):
            end_game(update, context)
            return
        else:
            find_next_user_to_add_money(message.group_id, query_data.arg2)
            update_keyboard(update, context)


def end_game_prepare(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    participant = utils.string_to_list(get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant"))
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权操作！", True)
        return
    if get_redis_dict_data(message.group_id, query_data.arg2, 0, "status") != GameStatus.ADDING_MONEY.value:
        utils.show_msg(update, "游戏已经结束", True)
        return
    now_user = get_redis_dict_data(message.group_id, query_data.arg2, 0, "now_user")
    if now_user != str(message.user_id):
        utils.show_msg(update, "当前没有轮到您操作，请等待！", True)
        return
    balance = utils.get_user_balance(message.user_id)
    rest_user_amount = 0
    last_add_money = int(get_redis_dict_data(message.group_id, query_data.arg2, 0, "last_add_money"))
    for user in participant:
        user_status = get_redis_dict_data(message.group_id, query_data.arg2, user, "status")
        if user_status == UserStatus.ADDED_MONEY.value or user == UserStatus.WAITING_ADD_MONEY.value:
            rest_user_amount += 1
    need_add_money = 0
    if rest_user_amount > 6:
        need_add_money = last_add_money * 5
        if balance < need_add_money:
            utils.show_msg(update, "您当前的余额不足，此局游戏剩余{}人，此次开牌需要跟注：${}".format(
                rest_user_amount, utils.balance_format(need_add_money)), True)
            return
    else:
        need_add_money = last_add_money * 3
        if balance < need_add_money:
            utils.show_msg(update, "您当前的余额不足，此局游戏剩余{}人，此次开牌需要跟注：${}".format(
                rest_user_amount, utils.balance_format(need_add_money)), True)
            return
    # 满足条件，游戏结束
    user_amount = int(get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "total_amount"))
    utils.reduce_user_balance(message.user_id, need_add_money)
    new_amount = user_amount + need_add_money
    update_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "total_amount", new_amount)
    end_game(update, context)
    return


def see_puke(update, context):
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    participant = utils.string_to_list(get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant"))
    if message.user_id not in participant:
        utils.show_msg(update, "您没有参与此局游戏，无权操作！", True)
        return
    if get_redis_dict_data(message.group_id, query_data.arg2, 0, "status") != GameStatus.ADDING_MONEY.value:
        utils.show_msg(update, "游戏已经结束", True)
        return
    # user_status = get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "status")
    look_status = get_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "look")
    if look_status == LookPuke.NOT_LOOK.value:
        update_redis_dict_data(message.group_id, query_data.arg2, message.user_id, "look", LookPuke.LOOKED.value)
        update_keyboard(update, context)
    utils.show_msg(update, "您的手牌为：\n" + show_puke(message.group_id, query_data.arg2, message.user_id), True)


def show_puke(group_id, game_id, user):
    puke1 = get_redis_dict_data(group_id, game_id, user, "puke1")
    puke2 = get_redis_dict_data(group_id, game_id, user, "puke2")
    puke3 = get_redis_dict_data(group_id, game_id, user, "puke3")
    text = ""
    text += num_to_color_symbol(puke3) + num_to_text(puke3) + " "
    text += num_to_color_symbol(puke2) + num_to_text(puke2) + " "
    text += num_to_color_symbol(puke1) + num_to_text(puke1) + " "
    return text


def calculate(update, context):
    print("calculate")
    message = utils.ana_message(update)
    query_data = utils.ana_query_data(update)
    show_text = ""
    reply_text = ""
    participant = utils.string_to_list(get_redis_dict_data(message.group_id, query_data.arg2, 0, "participant"))
    rest_user_level_list = []
    for user in participant:
        user_status = get_redis_dict_data(message.group_id, query_data.arg2, user, "status")
        if user_status == UserStatus.ADDED_MONEY.value or user_status == UserStatus.WAITING_ADD_MONEY.value:
            level = get_redis_dict_data(message.group_id, query_data.arg2, user, "level")
            rest_user_level_list.append([user, level])
    sorted_rest_user_level_list = sorted(rest_user_level_list, key=lambda kv: (int(kv[1])))  # level 升序排列
    level1 = sorted_rest_user_level_list[0][1]
    same_level_user = []
    for user in sorted_rest_user_level_list:
        if user[1] == level1:
            same_level_user.append(user[0])
    puke3_list = []
    for user in same_level_user:
        puke3 = get_redis_dict_data(message.group_id, query_data.arg2, user, "puke3")
        puke3_list.append([user, num_to_point(puke3)])
    sorted_puke3_user_list = sorted(puke3_list, key=lambda kv: int(kv[1]), reverse=True)
    max_puke3 = sorted_puke3_user_list[0][1]
    same_puke3_user = []
    for user_puke in sorted_puke3_user_list:
        if user_puke[1] == max_puke3:
            same_puke3_user.append(user_puke[0])
    puke2_list = []
    for user in same_puke3_user:
        puke2 = get_redis_dict_data(message.group_id, query_data.arg2, user, "puke2")
        puke2_list.append([user, num_to_point(puke2)])
    sorted_puke2_user_list = sorted(puke2_list, key=lambda kv: int(kv[1]), reverse=True)
    max_puke2 = sorted_puke2_user_list[0][1]
    same_puke2_user = []
    for user_puke in sorted_puke2_user_list:
        if user_puke[1] == max_puke2:
            same_puke2_user.append(user_puke[0])
    puke1_list = []
    for user in same_puke2_user:
        puke1 = get_redis_dict_data(message.group_id, query_data.arg2, user, "puke1")
        puke1_list.append([user, num_to_point(puke1)])
    sorted_puke1_user_list = sorted(puke1_list, key=lambda kv: int(kv[1]), reverse=True)
    max_puke1 = sorted_puke1_user_list[0][1]
    same_puke1_user = []
    for user_puke in sorted_puke1_user_list:
        if user_puke[1] == max_puke1:
            same_puke1_user.append(user_puke[0])
    # 比牌大小完毕 如果有多名用户胜出？
    win_user_list = same_puke1_user
    loser_list = []
    for user in participant:
        if user not in win_user_list:
            loser_list.append(user)
    for user in win_user_list:
        look = get_redis_dict_data(message.group_id, query_data.arg2, user, "look")
        win_amount = 0
        winner_total_amount = int(get_redis_dict_data(message.group_id, query_data.arg2, user, "total_amount"))
        for loser in loser_list:
            total_amount = int(get_redis_dict_data(message.group_id, query_data.arg2, loser, "total_amount"))
            part_amount = total_amount * (1.0 / float(len(win_user_list)))
            if look == LookPuke.LOOKED.value:
                win_amount += part_amount * 0.6
            else:
                win_amount += part_amount
        win_amount = int(win_amount)
        utils.add_user_balance(user, win_amount + winner_total_amount)
        show_text += "【[{}](https://t.me/gameyyds)】{}：{} {} *赢* \+${}\n".format(
            utils.get_title(user),
            get_redis_dict_data(message.group_id, query_data.arg2, user, "name"),
            show_puke(message.group_id, query_data.arg2, user),
            look_to_text(look),
            utils.balance_format(win_amount + winner_total_amount)
        )
        if win_amount >= 100000000:
            reply_text = reply_text + "【[{}](https://t.me/gameyyds)】{}：*赢* +${}\n".format(
                utils.get_title(user),
                get_redis_dict_data(message.group_id, query_data.arg2, user, "name"),
                utils.balance_format(win_amount)
            )
    for loser in loser_list:
        total_amount = int(get_redis_dict_data(message.group_id, query_data.arg2, loser, "total_amount"))
        lose_amount = 0
        look_loser = get_redis_dict_data(message.group_id, query_data.arg2, loser, "look")
        for winner in win_user_list:
            look = get_redis_dict_data(message.group_id, query_data.arg2, winner, "look")
            part_amount = total_amount * (1.0 / float(len(win_user_list)))
            if look == LookPuke.LOOKED.value:
                lose_amount += part_amount * 0.6
            else:
                lose_amount += part_amount
        utils.add_user_balance(loser, total_amount - lose_amount)
        show_text += "【[{}](https://t.me/gameyyds)】{}：{} {} *输* ~\-${}~\n".format(
            utils.get_title(loser),
            get_redis_dict_data(message.group_id, query_data.arg2, loser, "name"),
            show_puke(message.group_id, query_data.arg2, loser),
            look_to_text(look_loser),
            utils.balance_format(lose_amount)
        )
    show_text = "*炸金花* 本场结算\n\n" + show_text
    if len(reply_text) > 0:
        return show_text, reply_text, True
    else:
        return show_text, "", False


def generate_add_money_user(group_id, game_id):
    text = ""
    r = connector.get_connection(19)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, amount = r.hmget("{}_{}_{}".format(group_id, game_id, user), ["name", "total_amount"])
        text += "【[{}](https://t.me/gameyyds)】{}：${}".format(utils.get_title(user), name,
                                                             utils.balance_format(amount)) + "\n"
    return text[:-1]


def generate_choosing_user(group_id, game_id):
    text = ""
    r = connector.get_connection(19)
    participant = utils.string_to_list(r.hget("{}_{}".format(group_id, game_id), "participant"))
    for user in participant:
        name, total_amount, look, status = r.hmget("{}_{}_{}".format(group_id, game_id, user),
                                                   ["name", "total_amount", "look", "status"])
        text = text + "【[{}](https://t.me/gameyyds)】{}：{} {} ${}\n".format(
            utils.get_title(user),
            name,
            look_to_text(look),
            show_user_status(status),
            utils.balance_format(total_amount)
        )
    return text[:-1]


def check_circle(group_id, game_id, circle, now_user):
    gamed_user = 0
    participant = utils.string_to_list(get_redis_dict_data(group_id, game_id, 0, "participant"))
    now_user_order = int(get_redis_dict_data(group_id, game_id, now_user, "order"))
    for user in participant:
        user_order = int(get_redis_dict_data(group_id, game_id, user, "order"))
        user_status = get_redis_dict_data(group_id, game_id, user, "status")
        if user_order <= now_user_order:
            gamed_user += 1
        elif user_order > now_user_order and user_status == UserStatus.EXITED.value:
            gamed_user += 1
    if gamed_user == len(participant):
        update_redis_dict_data(group_id, game_id, 0, "circle", circle + 1)


def get_puke_level(p1, p2, p3):
    color1 = num_to_color_level(p1)
    color2 = num_to_color_level(p2)
    color3 = num_to_color_level(p3)
    point1 = num_to_point(p1)
    point2 = num_to_point(p2)
    point3 = num_to_point(p3)
    if point1 == point2 == point3:
        return 1  # 豹子
    elif (2 * point2 == point1 + point3) and (color1 == color2 == color3):
        return 2  # 同花顺
    elif check_level3(point1, point2, point3, color1, color2, color3):
        return 3  # 同花对
    elif color1 == color2 == color3:
        return 4  # 同花
    elif 2 * point2 == point1 + point3:
        return 5  # 顺子
    elif check_level6(point1, point2, point3):
        return 6
    else:
        return 7


def check_level6(point1, point2, point3):
    if (point1 == point2) or (point1 == point3) or (point2 == point3):
        return True
    else:
        return False


def check_level3(point1, point2, point3, color1, color2, color3):
    if color1 == color2 == color3:
        if (point1 == point2) or (point1 == point3) or (point2 == point3):
            return True
    else:
        return False


def is_game_over(group_id, game_id):
    participant = utils.string_to_list(get_redis_dict_data(group_id, game_id, 0, "participant"))
    rest_user = 0
    for user in participant:
        user_status = get_redis_dict_data(group_id, game_id, user, "status")
        if user_status == UserStatus.WAITING_ADD_MONEY.value or user_status == UserStatus.ADDED_MONEY.value:
            rest_user += 1
    if rest_user == 1:
        return True
    else:
        return False


def find_next_user_to_add_money(group_id, game_id):
    r = connector.get_connection(19)
    now_user = get_redis_dict_data(group_id, game_id, 0, "now_user")
    participant = utils.string_to_list(get_redis_dict_data(group_id, game_id, 0, "participant"))
    now_order = int(get_redis_dict_data(group_id, game_id, now_user, "order"))
    for user in participant:
        # 找到下一位用户
        print("下一位用户的order" + str(((now_order + 1) % len(participant))))
        print("该用户order {}".format(int(get_redis_dict_data(group_id, game_id, user, "order"))))
        print("该用户状态 {}".format(get_redis_dict_data(group_id, game_id, user, "status")))
        if int(get_redis_dict_data(group_id, game_id, user, "order")) == (now_order + 1) % len(participant) \
                and get_redis_dict_data(group_id, game_id, user, "status") == UserStatus.ADDED_MONEY.value:
            # print("找到下一位用户")
            update_redis_dict_data(group_id, game_id, 0, "now_user", user)
            update_redis_dict_data(group_id, game_id, user, "status",
                                   UserStatus.WAITING_ADD_MONEY.value)
            r.set("{}_{}_{}_time_limit".format(group_id, game_id, user), "", ex=ADD_MONEY_TIME_LIMIT)
            break
        elif int(get_redis_dict_data(group_id, game_id, user, "order")) == (now_order + 1) % len(participant) \
                and get_redis_dict_data(group_id, game_id, user, "status") == UserStatus.EXITED.value:
            print("该用户已经出局，跳过")
            now_order += 1


zjh2_handler = CommandHandler('zjh2', zjh2)
