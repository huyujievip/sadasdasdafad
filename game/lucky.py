import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
import connector
import utils
from config import TOKEN
import random
import re
import time
from telegram.ext.dispatcher import run_async

bot = telegram.Bot(token=TOKEN)


@run_async
def start_game(update, context):
    # update.message.reply_text("功能维护中，请等待重新上线！")
    # return
    try:
        message = utils.ana_message(update)
        if utils.is_user_in_black_list(message.user_id):
            update.message.reply_text("黑名单用户，无权操作")
            return
        if message.group_type == "private":
            utils.send_message(message.group_id, "此命令只能在群组中使用")
            return
        user_input = update.message.text
        rst = re.match("\/hb(.*bot)?\s(\d+)\s(\d+)", user_input)
        if rst is None:
            update.message.reply_text(
                "格式错误\n /hb 金额 个数\n金额不能小于10亿\n个数范围[10, 300]\n例如： /hb 1000000000 10")
            return
        balance = utils.get_user_balance_update(message.user_id, update)
        amount = int(rst.group(2))
        total_amount = amount
        if balance < total_amount:
            update.message.reply_text("余额不足！")
            return
        count = int(rst.group(3))
        if count < 10 or count > 300:
            update.message.reply_text("红包个数不能少于10个，且不能大于300个")
            return
        if amount < 1000000000 or amount > 9999999999999999999999:
            update.message.reply_text(
                "红包金额不能少于 $10,0000,0000")
            return
        utils.reduce_user_balance(message.user_id, total_amount)
        try:
            min_scale = 100000
            remain_size = count
            remain_amount = round(amount / min_scale)
            # min_amount = round(remain_amount / count / 2)
            middle_amount = round(remain_amount / count)
            # print("remain_amount: {}, min_amount: {}, middle_amount: {}".format(remain_amount, min_amount, middle_amount))

            random.seed(time.time())
            red_list = []
            while 1:
                if remain_size == 1:
                    red_list.append(remain_amount*min_scale)
                    break
                temp_amount = round(remain_amount/remain_size/2)+random.randint(0, round(remain_amount/remain_size))
                red_list.append(temp_amount*min_scale)
                remain_size -= 1
                remain_amount -= temp_amount

            red_list[random.randint(0, count - 1)] += amount % min_scale
            print(red_list)
            r = connector.get_connection(2)
            game_id = utils.get_timestamp()
            print("此局游戏ID：{}".format(game_id))
            print("群组ID：{}".format(message.group_id))
            print("lucky_{}_{}".format(message.group_id, game_id))
            print("lucky_{}_{}_amount".format(message.group_id, game_id))
            for i in red_list:
                r.rpush("lucky_{}_{}".format(message.group_id, game_id), i)
            r.set("lucky_{}_{}_amount".format(
                message.group_id, game_id), total_amount)
            r.set("lucky_{}_{}_master".format(
                message.group_id, game_id), message.name)
            r.set("lucky_{}_{}_master_id".format(
                message.group_id, game_id), message.user_id)
            lucky_count = r.llen(
                "lucky_{}_{}".format(message.group_id, game_id))
            print(lucky_count)
            keyboard = [
                [
                    InlineKeyboardButton("🧧抢红包", callback_data=str(
                        "lucky_{}_openRedPocket_{}_None_None_None".format(message.group_id, game_id)))
                ]
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            bot.send_message(
                chat_id=message.group_id,
                text="{}\n"
                     "发了个 \#红包\n"
                     "金额：{}\n"
                     "红包个数：{}\n\n"
                     "{}".format(message.name, utils.balance_format(
                    total_amount), lucky_count, utils.get_random_ad()),
                reply_markup=reply_markup,
                parse_mode="MarkdownV2",
                disable_web_page_preview=True
            )
        except Exception as e:
            print(e)
            print("红包发送失败，返回余额")
            utils.add_user_balance(message.user_id, total_amount)
    except Exception as e:
        print(e)


def open_read_pocket(update):
    try:
        query = update.callback_query
        message = utils.ana_message(update)
        query_data = utils.ana_query_data(update)
        print("此局游戏ID：{}".format(query_data.arg1))
        r = connector.get_connection(2)
        if not r.exists("lucky_{}_{}".format(message.group_id, query_data.arg1)):
            # 拦截后续用户的操作
            utils.show_msg(update, "红包已经被抢完啦", True)
            return
        if not r.exists("lucky_{}_{}_user_list".format(message.group_id, query_data.arg1)):
            r.set("lucky_{}_{}_user_list".format(message.group_id, query_data.arg1),
                  utils.list_to_string([message.user_id]))
        else:
            user_list = utils.string_to_list(
                r.get("lucky_{}_{}_user_list".format(message.group_id, query_data.arg1)))
            if message.user_id in user_list:
                utils.show_msg(update, "您已经抢过这个红包，无法再次领取", True)
                return
            user_list.append(message.user_id)
            r.set("lucky_{}_{}_user_list".format(message.group_id, query_data.arg1),
                  utils.list_to_string(user_list))
        master_name = r.get("lucky_{}_{}_master".format(
            message.group_id, query_data.arg1))
        amount = r.get("lucky_{}_{}_amount".format(message.group_id, query_data.arg1))
        print("红包总金额：{}".format(amount))
        lucky_amount = int(r.lpop("lucky_{}_{}".format(
            message.group_id, query_data.arg1)))
        utils.add_user_balance(message.user_id, lucky_amount)
        utils.show_msg(update, "恭喜你，抢到 ${}".format(utils.balance_format(lucky_amount)), True)
        r.rpush("lucky_{}_{}_history".format(message.group_id, query_data.arg1),
                utils.list_to_string([message.user_id, message.name, lucky_amount]))
        lucky_count = int(r.llen("lucky_{}_{}".format(
            message.group_id, query_data.arg1)))
        keyboard = [
            [
                InlineKeyboardButton("抢红包", callback_data=str(
                    "lucky_{}_openRedPocket_{}_None_None_None".format(message.group_id, query_data.arg1)))
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        if lucky_count == 0:
            utils.show_msg(update, "红包已经被抢完啦", True)
            try:
                query.edit_message_text(
                    text="{}\n"
                         "发了个 \#红包\n"
                         "数额：{}\n\n"
                         "{}\n\n"
                         "{}".format(master_name, utils.balance_format(amount),
                                     generate_red_pocket_history(message.group_id, query_data.arg1),
                                     utils.get_random_ad()),
                    parse_mode="MarkdownV2",
                    disable_web_page_preview=True
                )
                r.delete("lucky_{}_{}".format(
                    message.group_id, query_data.arg1))
                r.delete("lucky_{}_{}_user_list".format(
                    message.group_id, query_data.arg1))
                r.delete("lucky_{}_{}_amount".format(
                    message.group_id, query_data.arg1))
                r.delete("lucky_{}_{}_master".format(
                    message.group_id, query_data.arg1))
                r.delete("lucky_{}_{}_master_id".format(
                    message.group_id, query_data.arg1))
                r.delete("lucky_{}_{}_history".format(
                    message.group_id, query_data.arg1))
            except Exception as e:
                print(e)
            return
        # print("设置刷新锁定")
        # print(r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)))
        # if r.exists("{}_{}_refresh_lock".format(message.group_id, query_data.arg2)):
        #     print("存在锁定，跳过刷新")
        #     return
        # query.edit_message_text(
        #     text="{}\n"
        #          "发了个 \#红包\n"
        #          "数额：{}\n"
        #          "剩余个数：{}\n\n"
        #          "{}\n\n"
        #          "{}".format(master_name, utils.balance_format(amount), lucky_count,
        #                      generate_red_pocket_history(message.group_id, query_data.arg1), utils.get_random_ad()),
        #     reply_markup=reply_markup,
        #     parse_mode="MarkdownV2",
        #     disable_web_page_preview=True
        # )
        # r.set("{}_{}_refresh_lock".format(message.group_id, query_data.arg2), "", ex=2)
    except Exception as e:
        print(e)


def generate_red_pocket_history(group_id, game_id):
    try:
        r = connector.get_connection(2)
        lucky_count = int(r.llen("lucky_{}_{}".format(group_id, game_id)))
        open_recodes = r.lrange(
            "lucky_{}_{}_history".format(group_id, game_id), 0, -1)
        # open_amount = int(r.llen("lucky_{}_{}_history".format(group_id, game_id)))
        text = "领取详情\n"
        for recode in open_recodes:
            converted = utils.string_to_list(recode)
            user_id = converted[0]
            username = converted[1]
            amount = converted[2]
            text += "【[{}](https://t.me/gameyyds)】{}：${}\n".format(utils.get_title(user_id), username,
                                                                   utils.balance_format(amount))
        # 如果已经领完，生成手气王
        if lucky_count == 0:
            king_name = ""
            king_amount = 0
            for recode in open_recodes:
                converted = utils.string_to_list(recode)
                if int(converted[2] > king_amount):
                    king_name = converted[1]
                    king_amount = int(converted[2])
            text += "\n🎉本场游戏的手气王是：\n{}：${}".format(
                king_name, utils.balance_format(king_amount))
        print(text)
        return text
    except Exception as e:
        print(e)


def process(update, context):
    query = update.callback_query
    try:
        game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(
            update).params_to_list()
        if action == "openRedPocket":
            open_read_pocket(update)
    except Exception as e:
        print(e)
        query.answer(text="发生错误，请稍后重试")


lucky_handler = CommandHandler("hb", start_game)
