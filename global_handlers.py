import telegram
from telegram.ext import CommandHandler, CallbackQueryHandler, MessageHandler, Filters
import connector
from game import lp, pj, lucky, yc, yx, bj, hh, dt, erba, bg, jisusuoha, bk, bs, wk, zjh, zjh2, zjh3
import utils
from config import TOKEN, ADMIN_ID, ZZ_MAX_AMOUNT, SUPER_ADMIN, RANK_EXCLUDE_LIST
import re

bot = telegram.Bot(token=TOKEN)


def set_title(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if message.group_type != "private":
        utils.send_message(message.group_id, "此命令只能私聊使用")
        return
    user_input = update.message.text
    rst = re.match("\/settitle\s(\w+)", user_input)
    if rst is not None:
        title = rst.group(1)
        title = utils.escaped_text(title)
        if len(title) > 4:
            utils.send_message(message.user_id, "称号长度不能超过4，突破限制请联系管理员，当前长度：{}".format(len(title)))
            return
        if title in ["一贫如洗", "囊中羞涩", "自给自足", "丰衣足食", "小康之家", "一掷千金", "腰缠万贯", "富可敌国", "宇宙首富"]:
            utils.send_message(message.user_id, "称号不能与系统默认称号相同")
            return
        balance = utils.get_user_balance(message.user_id)
        if balance < 0:
            utils.send_message(message.user_id, "余额不足\n设置称号需要花费 $0")
            return
        utils.reduce_user_balance(message.user_id, 0)
        r = connector.get_connection(0)
        r.hset(message.user_id, "title", title)
        utils.send_message(message.user_id, "称号设置完成\n您的当前称号：{}".format(title))
    else:
        bot.sendMessage(
            chat_id=message.user_id,
            text="自定义称号功能使用说明\n"
                 "私聊Bot使用如下指令格式\n"
                 "/settitle 自定义称号\n"
                 "比如\n"
                 "`/settitle 高级玩家`\n"
                 "使用限制\n"
                 "称号字符长度大于零小于等于4\n"
                 "不可自定义为系统默认称号\n"
                 "常规称呼免费修改，突破限制请联系管理员",
            parse_mode="Markdown"
        )


def zz(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if message.group_type == "private":
        utils.send_message(message.group_id, "此命令只能在群组中使用")
        return
    user_input = update.message.text
    rst = re.match("\/zz(.*?bot)?\s(\d+)", user_input)
    if rst is None:
        update.message.reply_text("格式错误\n请回复需要转账对象的消息，然后输入转账指令：\n/zz 转账数额\n每笔转账需要扣除8%手续费")
        return
    amount = int(rst.group(2))
    try:
        target_id = str(update.message.reply_to_message.from_user.id)
        is_bot = str(update.message.reply_to_message.from_user.is_bot)
    except Exception as e:
        update.message.reply_text("格式错误，请回复需要转账对象的消息\n然后输入转账指令：\n/zz 转账数额\n每笔转账需要扣除8%手续费")
        return
    if is_bot == "True":
        update.message.reply_text("不能给机器人转账")
        return
    if not utils.check_user_exists(target_id):
        update.message.reply_text("此用户在系统中无记录，无效转账")
        return
    if target_id == str(message.user_id):
        update.message.reply_text("不能给自己转账！")
        return
    target_match = re.match(r"^\d+$", target_id)
    if target_match is None:
        update.message.reply_text("格式错误，请回复需要转账对象的消息\n然后输入转账指令：\n/zz 转账数额\n每笔转账需要扣除8%手续费")
        return
    master_balance = utils.get_user_balance(message.user_id)
    target_balance = utils.get_user_balance(target_id)
    if master_balance < amount:
        update.message.reply_text(
            "余额不足，转账数额：{}".format(utils.balance_format(amount)))
        return
    utils.reduce_user_balance(message.user_id, amount)
    utils.add_user_balance(target_id, int(float(amount) * 0.92))
    update.message.reply_text("转账成功，实际到账：{}，手续费：{}".format(utils.balance_format(int(float(amount) * 0.92)),
                                                           utils.balance_format(int(float(amount) * 0.08))))
    if amount > ZZ_MAX_AMOUNT:
        for admin in SUPER_ADMIN:
            try:
                utils.send_message(
                    admin, "请注意， {} 向 {} 转账 {}".format(message.user_id, target_id, utils.balance_format(amount))
                )
            except Exception as e:
                print("向管理员推送信息失败")
                print(e)


# @run_async
def rank(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if int(message.user_id) in RANK_EXCLUDE_LIST:
        return
    try:
        list1 = utils.rank_list(update, context)
        for i in list1:
            print(i)
        if len(list1) == 0:
            utils.send_message(message.group_id, "目前没有玩家的财富超过 $1,0000,0000\n继续努力吧")
            return
        text = '*🏆TG娱乐超级富豪排行榜🏆：*\n\n'
        order = 1
        for i in list1:
            text += "*{}\. {}：${}*\n".format(order, i[0], utils.balance_format(i[1]))
            order += 1
        text += "\n*🔹榜单由所有玩家积分进行排名并显示积分最多的20名*\n*🔹榜单每十分钟更新一次*\n*🔹您当前的排名：{}*".format(utils.get_balance_order(message.user_id))
        update.message.reply_text(text, parse_mode="MarkdownV2")
    except Exception as e:
        print(e)


# @run_async
def shen(update, context):
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        update.message.reply_text("黑名单用户，无权操作")
        return
    if int(message.user_id) in RANK_EXCLUDE_LIST:
        utils.show_msg(update, "隐藏用户，无法进行游戏，请您不要暴露自己的积分。", True)
    try:
        message = utils.ana_message(update)
        list2 = utils.shen_list(update, context)
        text = '*🏆TG娱乐赌神大赛排行榜🏆*\n\n'
        order = 1
        symbol_list = ["", "🥇", "🥈", "🥉", "🏅", "🏅", "🎖", "🎖", "🎖", "🎖", "🎖"]
        for i in list2:
            text += "{}*{}\. {}：x{}*\n".format(symbol_list[order], order, i[0], utils.balance_format(i[1]))
            order += 1
        text += "\n*🔹榜单由玩家有效参与游戏结算产生每分钟统计一次*\n*🔹榜单每十分钟更新一次，每周一凌晨8点重置*\n*🔹玩家胜场结算金额 ≥2W 进入此榜单*\n"
        # print(text)
        update.message.reply_text(text, parse_mode="MarkdownV2")
    except Exception as e:
        print(e)


def add_user_to_bl(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/lh\s(\d+)", user_input)
    if rst is not None:
        user_id = str(rst.group(1))
        r = connector.get_connection(10)
        redis_black_list = utils.string_to_list(r.get("black_list"))
        if str(user_id) in redis_black_list:
            update.message.reply_text("用户已存在于黑名单")
        else:
            redis_black_list.append(user_id)
            # 写回 Redis
            r.set("black_list", str(redis_black_list))
            update.message.reply_text("添加成功")
            if int(message.user_id) not in SUPER_ADMIN:
                for user in SUPER_ADMIN:
                    try:
                        utils.send_message(user, "#lh \n管理操作记录：\n{} 将 {} 拉入黑名单".format(message.user_id,
                                                                                       user_id))
                    except Exception as e:
                        print("管理员命令推送失败")
                        print(e)
    else:
        update.message.reply_text("格式错误!\n/lh 用户ID\n")


def remove_user_from_bl(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/jb\s(\d+)", user_input)
    if rst is not None:
        user_id = str(rst.group(1))
        r = connector.get_connection(10)
        redis_black_list = utils.string_to_list(r.get("black_list"))
        if str(user_id) in redis_black_list:
            redis_black_list.remove(str(user_id))
            # 写回 Redis
            r.set("black_list", str(redis_black_list))
            update.message.reply_text("移除成功")
            if int(message.user_id) not in SUPER_ADMIN:
                for user in SUPER_ADMIN:
                    try:
                        utils.send_message(user,
                                           "#jb \n管理操作记录：\n{} 将 {} 从黑名单中移除".format(message.user_id, user_id))
                    except Exception as e:
                        print("管理员命令推送失败")
                        print(e)
        else:
            update.message.reply_text("用户不存在黑名单中")
    else:
        update.message.reply_text("格式错误!\n/jb 用户ID\n")


def admin_add_balance(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/zengjia\s(\d+)\s(\d+)", user_input)
    if rst is not None:
        user_id = str(rst.group(1))
        amount = int(rst.group(2))
        utils.add_user_balance(user_id, amount * 10000)
        update.message.reply_text(
            "增加成功\n增加数量：{}\n用户 `{}` 当前余额：{}".format(utils.balance_format(amount * 10000), user_id,
                                                    utils.balance_format(utils.get_user_balance(user_id))),
            parse_mode="Markdown"
        )
        if int(message.user_id) not in SUPER_ADMIN:
            for user in SUPER_ADMIN:
                try:
                    utils.send_message(user, "#zengjia \n管理操作记录：\n{} 为 {} 增加了余额，数量为：{}".format(message.user_id,
                                                                                               user_id,
                                                                                               utils.balance_format(
                                                                                                   amount * 10000)))
                except Exception as e:
                    print("管理员命令推送失败")
                    print(e)
    else:
        update.message.reply_text("格式错误!\n/zengjia 用户ID 数量(单位：万)\n")


def admin_reduce_balance(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/kouchu\s(\d+)\s(\d+)", user_input)
    if rst is not None:
        user_id = str(rst.group(1))
        amount = int(rst.group(2))
        balance = int(utils.get_user_balance(user_id))
        if balance < amount * 10000:
            update.message.reply_text("余额不足，无法减少，该用户当前余额：{}".format(utils.balance_format(balance)))
            return
        utils.reduce_user_balance(user_id, amount * 10000)
        update.message.reply_text(
            "扣除成功\n扣除数量：{}\n用户 `{}` 当前余额：{}".format(utils.balance_format(amount * 10000), user_id,
                                                    utils.balance_format(utils.get_user_balance(user_id))),
            parse_mode="Markdown"
        )
        if int(message.user_id) not in SUPER_ADMIN:
            for user in SUPER_ADMIN:
                try:
                    utils.send_message(user, "#kouchu \n管理操作记录：\n{} 扣除了 {} 的余额，数量为：{}".format(message.user_id,
                                                                                              user_id,
                                                                                              utils.balance_format(
                                                                                                  amount * 10000)))
                except Exception as e:
                    print("管理员命令推送失败")
                    print(e)
    else:
        update.message.reply_text("格式错误!\n/kouchu 用户ID 数量(单位：万)\n")


def show_user_data(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    r = connector.get_connection(0)
    user_list = r.keys("*")
    total_user = 0
    active_user = 0
    for user in user_list:
        total_user += 1
        name, balance, id = r.hmget(user, ["name", "balance", "id"])
        balance = int(balance)
        if balance > 0:
            active_user += 1
    utils.send_message(message.user_id, "当前总用户数：{}\n余额不为0的用户数：{}".format(total_user, active_user))
    if int(message.user_id) not in SUPER_ADMIN:
        for user in SUPER_ADMIN:
            try:
                utils.send_message(user, "#info \n管理操作记录：\n{} 查看了系统数据".format(message.user_id))
            except Exception as e:
                print("管理员命令推送失败")
                print(e)


def group_info(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in ADMIN_ID and int(message.user_id) not in SUPER_ADMIN:
        return
    r = connector.get_connection(1)
    g_set = set()
    for i in r.keys():
        rst = re.match(r"-(\d+)_\d+$", i)
        if rst is not None:
            g_set.add(rst.group(1))
    update.message.reply_text("当前群组：{}个".format(len(g_set)))


def all_user_reduce_money(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/allkouchu\s(\d+)", user_input)
    if rst is not None:
        amount = int(rst.group(1))
        if amount - 1 < 0:
            update.message.reply_text("格式错误!\n/allkouchu 数量\n(除法操作，输入10000，即所有用户去掉4个0)")
        r = connector.get_connection(0)
        user_list = r.keys("*")
        for user in user_list:
            balance, id = r.hmget(user, ["balance", "id"])
            new_balance = int(int(balance) / amount)
            r.hset(user, "balance", new_balance)
        bank_db = connector.get_connection(11)
        bank_user_list = bank_db.keys()
        for bank_user in bank_user_list:
            ck = bank_db.get(bank_user)
            new_ck = int(int(ck) / amount)
            bank_db.set(bank_user, new_ck)
        update.message.reply_text("操作成功", parse_mode="Markdown")
    else:
        update.message.reply_text("格式错误!\n/allkouchu 数量\n(除法操作，输入10000，即所有用户去掉4个0)")


def all_user_add_money(update, context):
    message = utils.ana_message(update)
    if int(message.user_id) not in SUPER_ADMIN:
        return
    user_input = update.message.text
    rst = re.match(r"\/allzengjia\s(\d+)", user_input)
    if rst is not None:
        amount = int(rst.group(1))
        print(amount)
        if amount - 1 < 0:
            update.message.reply_text("格式错误!\n/allzengjia 数量\n(乘法操作，输入10000，即所有用户增加4个0)")
        r = connector.get_connection(0)
        user_list = r.keys("*")
        for user in user_list:
            balance, id = r.hmget(user, ["balance", "id"])
            new_balance = int(int(balance) * amount)
            r.hset(user, "balance", new_balance)
        bank_db = connector.get_connection(11)
        bank_user_list = bank_db.keys()
        for bank_user in bank_user_list:
            ck = bank_db.get(bank_user)
            new_ck = int(int(ck) * amount)
            bank_db.set(bank_user, new_ck)
        update.message.reply_text("操作成功", parse_mode="Markdown")
    else:
        update.message.reply_text("格式错误!\n/allzengjia 数量\n(乘法操作，输入10000，即所有用户增加4个0)")


def test_func(update, context):
    try:
        update.message.reply_text(
            "[\[广告\]][🆚显示文字](https://baidu.com)",
            parse_mode="MarkdownV2"
        )
    except Exception as e:
        print(e)


def global_button_dispatcher(update, context):
    query = update.callback_query
    message = utils.ana_message(update)
    if utils.is_user_in_black_list(message.user_id):
        utils.show_msg(update, "黑名单用户，无权操作", True)
        return
    if query.data == "show_balance":
        utils.show_balance(update, context)
        # utils.show_msg(update, "功能修复中，等候重新上线", True)
    elif query.data == "sign":
        utils.sign(update, context)
    game_name, group_id, action, arg1, arg2, arg3, arg4 = utils.ana_query_data(update).params_to_list()
    if game_name == "lp":
        lp.process(update, context)
    elif game_name == "lucky":
        lucky.process(update, context)
    elif game_name == "yc":
        yc.process(update, context)
    elif game_name == "dt":
        dt.process(update, context)
    elif game_name == "pj":
        pj.process(update, context)
    elif game_name == "yx":
        yx.process(update, context)
    elif game_name == "hh":
        hh.process(update, context)
    elif game_name == "bj":
        bj.process(update, context)
    elif game_name == "28":
        erba.process(update, context)
    elif game_name == "bg":
        bg.process(update, context)
    elif game_name == "jisusuoha":
        jisusuoha.process(update, context)
    elif game_name == "bk":
        bk.process(update, context)
    elif game_name == "bs":
        bs.process(update, context)
    elif game_name == "zjh":
        zjh.process(update, context)
    elif game_name == "zjh2":
        zjh2.process(update, context)
    elif game_name == "zjh3":
        zjh3.process(update, context)


set_title_handler = CommandHandler("settitle", set_title)
test_handler = CommandHandler("test", test_func)
shen_handler = CommandHandler("shen", shen)
zz_handler = CommandHandler("zz", zz)
rank_handler = CommandHandler("rank", rank)
lh_handler = CommandHandler("lh", add_user_to_bl)
jb_handler = CommandHandler("jb", remove_user_from_bl)
user_info_handler = CommandHandler("info", show_user_data)
admin_balance_add_handler = CommandHandler("zengjia", admin_add_balance)
admin_all_balance_add_handler = CommandHandler("allzengjia", all_user_add_money)
admin_balance_reduce_handler = CommandHandler("kouchu", admin_reduce_balance)
admin_all_balance_reduce_handler = CommandHandler("allkouchu", all_user_reduce_money)
g_info_handler = CommandHandler("ginfo", group_info)
global_button_dispatcher_handler = CallbackQueryHandler(global_button_dispatcher)
